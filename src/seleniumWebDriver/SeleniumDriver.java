package seleniumWebDriver;

import org.openqa.selenium.WebDriver;

/**
 * @author rkennedy
 * 
 */
public class SeleniumDriver {

	public enum BrowserType {
		IE, CH, FF
	}

	private static WebDriver driver;
	
	
	public static WebDriver setDriver( BrowserType browser ) {
		if (browser.name().equalsIgnoreCase("ch")) {
			driver = ChromeDriverSingleton.getChromeDriver();
		} else if (browser.name().equalsIgnoreCase("ie")) {
			driver = InternetExplorerDriverSingleton.getInternetExplorerDriver();
		} else if (browser.name().equalsIgnoreCase("ff")) {
			driver = FireFoxDriverSingleton.getFireFoxDriver();
		} else {
			driver = null;
		}
		return driver;
	}
	
	/**
	 * 
	 * 
	 * @return
	 */
	public static WebDriver getDriver() {
		return driver;
	}
}
