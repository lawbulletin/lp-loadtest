package seleniumWebDriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public final class InternetExplorerDriverSingleton {

	private static WebDriver internetExplorerDriver = null;

	/**
	 * @return
	 */
	protected static WebDriver getInternetExplorerDriver() {
		if (internetExplorerDriver != null) {
			return internetExplorerDriver;
		}
		System.setProperty("webdriver.ie.driver",
		 "C:\\Java Libraries\\IEDriverServer.exe");
		
		internetExplorerDriver = new InternetExplorerDriver();
		internetExplorerDriver.manage().timeouts()
				.implicitlyWait(60, TimeUnit.SECONDS);
		return internetExplorerDriver;
	}

	/**
	 * Quits the Internet Explorer Driver, clears the cache and nullifies the
	 * driver object.
	 */
	protected static void quit() {
		if (internetExplorerDriver != null) {
			internetExplorerDriver.manage().deleteAllCookies();
			internetExplorerDriver.quit();
			internetExplorerDriver = null;
		}
	}
}
