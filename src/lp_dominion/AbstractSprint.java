package lp_dominion;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import pagefactorylibrary.DirectoryPageFactory;
import pagefactorylibrary.HomePageFactory;
import pagefactorylibrary.JVRPageFactory;
import pagefactorylibrary.LPMainPageFactory;
import pagefactorylibrary.LoginPageFactory;
import seleniumWebDriver.SeleniumDriver;
import seleniumWebDriver.SeleniumDriver.BrowserType;

import com.google.common.base.Function;





/**
 * 
 * @author hipatel + bgalatzer-levy
 *
 */

public class AbstractSprint{

	public static Boolean condition1;
	public static Boolean condition2;
	public static WebElement e;
	public static WebDriverWait tock;
	
	@BeforeClass
	public static void setUp() {
		LoginPageFactory log = PFCaller.loginFactory();
		SeleniumDriver.setDriver(BrowserType.FF); 
		forceWait(2);
		
		LoginCredentials.inProd(); // WHICH ENVIRONMENT? 
		
		SeleniumDriver.getDriver().get(LoginCredentials.getWebsite());
		
		SeleniumDriver.getDriver().manage().window().maximize();

//		log.getSubmitButton().click();
//		log.getUsername().sendKeys(LoginCredentials.getUsername());
//		log.getPassword().sendKeys(LoginCredentials.getPassword());
//		log.getClient().sendKeys(LoginCredentials.getClient()); //Not available in beta, manually comment this line for beta testing
//		log.getSignInButton().click();
	}
	
	public static void logIn(){
		LoginPageFactory lp = PFCaller.loginFactory();
		LPMainPageFactory m =   PFCaller.lpMainFactory();
		HomePageFactory home = PFCaller.homeFactory();
		int j=0;
		
		if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://wwwqa.lawyerport.com/")){
			LoginCredentials.inQA();
			j =1;
		}
		else if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://www.lawyerport.com/")){
			LoginCredentials.inProd();
			j =2;
		}
		else if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://wwwdev.lawyerport.com/")){
			LoginCredentials.inDev();
			j =3;
		}
		
		System.out.println("Logged in as : "+LoginCredentials.getUsername());
		SeleniumDriver.getDriver().get(LoginCredentials.getWebsite());
		forceWait(1);	
		
		

		home.getsignInButton().click();
		forceWait(1);	
		
		lp.getUsername().sendKeys(LoginCredentials.getUsername());
				lp.getPassword().sendKeys(LoginCredentials.getPassword());
				lp.getClient().sendKeys(LoginCredentials.getClient());
				
		if (j >= 1) {
			lp.getSubmitButton().click();
		} else {
			lp.getResetButton().click();
		}
					
				forceWait(1);
				if(SeleniumDriver.getDriver().getCurrentUrl().contains("redirect")){
					System.err.println("Log-in Redicrted to wrong page (Successful log-in page) attempting to resolve error");
					m.headerLogo().click();
					forceWait(1);
				}
	}
//				if(SeleniumDriver.getDriver().findElement(By.id("signin-button")).isDisplayed()){
//					System.err.println("Log-in Redicrted to wrong page (No-login homepage) attempting to resolve error");
//					SeleniumDriver.getDriver().findElement(By.id("signin-button")).click();
//					forceWait(1);
//				}
//			forceWait(1);
	
	public Boolean getText(String text){
		
		Boolean found = SeleniumDriver.getDriver().getPageSource().contains(text);
		
		return found;
	}
	
	
	public static String getLoginURL(){
		String login;
		
		login = (LoginCredentials.getWebsite() + "lb-cas/login");
		return login;
	}
//	@Before
//	public static void startTest(){
//		LPMainPageFactory lp = PageFactory.initElements(SeleniumDriver.getDriver(), LPMainPageFactory.class);
//		lp.headerLogo().click();
//		forceWait(2);
//	}
	
	@After
	public void endTest(){
		forceWait(1);
	}
	
	
	
	public static void tearDown(){
		LoginPageFactory lp = PFCaller.loginFactory();
		LPMainPageFactory m =   PFCaller.lpMainFactory();
		HomePageFactory home = PFCaller.homeFactory();
		
		
	
//		SeleniumDriver.getDriver().get(LoginCredentials.getWebsite());
//		if(home.getsignInButton().isDisplayed()){
//			SeleniumDriver.getDriver().close();
//		}
//		else{
//		lp.getLogoutButton()
//		m.logoutLink().click();
//		SeleniumDriver.getDriver().manage().deleteAllCookies();
//		forceWait(5);
//		SeleniumDriver.getDriver().quit();
//		}
		
		if(SeleniumDriver.getDriver() != null){
			forceWait(1);
			if(m.getHomeIcon().isDisplayed()){
//				SeleniumDriver.getDriver().close();
				m.getWelcomeLink().click();
				forceWait(2);
				m.getLogoutLink().click();
				SeleniumDriver.getDriver().manage().deleteAllCookies();
				forceWait(2);
				SeleniumDriver.getDriver().quit();
			   }
			
//			SeleniumDriver.getDriver().manage().deleteAllCookies();
//			forceWait(2);
//			SeleniumDriver.getDriver().close();

//	        try
//	        {
//	            Thread.sleep(5000);
//	            SeleniumDriver.getDriver().quit();
//	        }
//	        catch(Exception e)
//	        {
//	        }
		}
		
	}
	
	public String getURL(){
		
		return SeleniumDriver.getDriver().getCurrentUrl();
	}
	
		
		public static void forceWait(int sleepTime){
		
	//	System.out.println("human wait of: " + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
		public static String getString(WebElement e){
			String temp = e.getText().toString().trim();
			return temp;
		}
		
		public void goToURL(String URL){
			
			SeleniumDriver.getDriver().navigate().to(URL);
		}
		
		public void goBackUsingBrowserBack(){
			
			SeleniumDriver.getDriver().navigate().back();
			
	
		}
		
		public void scrollDown(){
			
			WebDriver driver = SeleniumDriver.getDriver();
			
			((JavascriptExecutor)driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		}
	
		/**
	 * returns number of search results from a court docket search, if any
	 * @throws Exception
	 */
	public static void cdSearchResults() throws Exception{
		
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) != null)
			System.out.print("Search Successful: ");
			String result = waitForElement(By.id("resultCount")).getText().toString();
			result = result.substring(25);
		
			System.out.println(result);
			System.out.print(" results returned");
		//	stupidHumanWait(1);
		if(ExpectedConditions.presenceOfElementLocated(By.id("subTabContent2")) != null)	
			if(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(.,'Return to the Search Form')]")) != null)
			System.out.println("No Results Found");
			else System.out.println("Search failed");
		
		
	}
	
public static int cdSearchResult() throws Exception{
		
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) != null)
			System.out.print("Search Successful ");
			String result = waitForElement(By.id("resultCount")).getText().toString();
			result = result.substring(25).trim();
			int results = Integer.parseInt(result); 
			System.out.println(results);
			System.out.println(" results returned");
		//	stupidHumanWait(1);
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) == null)	
			if(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(.,'Return to the Search Form')]")) != null)
			System.out.println("No Results Found");
			else System.out.println("Search failed");
		
		return results;
	}
	public void docketDateSearch() throws Exception {
		waitForElement(By.id("navRes")).click();
		waitForElement(By.xpath("//a[contains(.,'Court Dockets')]")).click();
		
		System.out.println("Court docket date search");
		waitForElement(By.id("form-field-6")).sendKeys(randomLastName()); // Defendant
	
		waitForElement(By.id("form-field-1")).sendKeys(getDate(-100, "MM/dd/yyyy")); // start date
		Thread.sleep(2000); // because by default end date will get outputed  
		waitForElement(By.id("form-field-2")).clear(); // end date 
		waitForElement(By.id("form-field-2")).sendKeys(getDate(5, "MM/dd/yyyy")); // end date 	
		Thread.sleep(10000);
		waitForElement(By.id("court-dockets-run-advanced-search")).click();
		//clickDocketResult();
	}
		//waits for an element for to appear on page, the default time is 30 seconds.
	//default time to wait for element is 30 seconds.
	public static WebElement waitForElement(final By Locator) throws Exception {
		 WebDriverWait wait = new WebDriverWait(SeleniumDriver.getDriver(), 30);

	    WebElement function = wait.until(new Function<WebDriver, WebElement>() {
	        public WebElement apply(WebDriver driver) {
	            return driver.findElement(Locator);
	        }
	    });

	    return  function;
	}
	
	// after a search is perform this method is to called to click on search results. 
	//Directory Page Only
	public void clickResult() throws Exception {
		try  { 
    		WebElement searchRow = waitForElement(By.xpath("//div[contains(@class,'search-row row-detail')]"));
			WebElement test2 = searchRow.findElement(By.className("search-detail-container")); 
			List<WebElement> clickName = test2.findElements(By.xpath("//a[contains(@class,'search-click-through')]"));
			int num = (int)(Math.random() * clickName.size()); 
			Thread.sleep(2000);
			String text = clickName.get(num).getText(); 
			System.out.println("clicking on Serach Result: " + text );
			clickName.get(num).click();
			Thread.sleep(5000);
		}catch (IndexOutOfBoundsException  t ){
			System.out.println("No results found, please refine your search");	
	//	} catch (Exception e) {
		//	email("No results found, please refine your search " + e.getCause());
		}
			//System.out.println("No results found, please refine your search"); 
	}
	
	//the purpose of this methods is to randomly select one of the options to do a search for organization in directory page
		public static String orgKeyWords(){
			String kewWords[] = {"ASSOCIATION", "CIVIC", "CORPORATION", "COURT", "GOVERNMENT", "LAWFIRM", "OTHER", "SCHOOL" }; 
			Random randWords = new Random(); 
			int select = randWords.nextInt(kewWords.length);
			System.out.println("Organization Type: " + kewWords[select] );
			return kewWords[select];
		}
		
		// using random function to select random name from the firstName Array
		public static String randomfistName() {
			String firstName[] = {"a", "b", "c", "d", "e"};
			Random randomFName = new Random();
			int select = randomFName.nextInt(firstName.length);
			System.out.println("Randomly selected first name: " + firstName[select]); 
			return firstName[select];  
		}
		
		// using random function to select random name from the lastName Array 
//		public static String randomLastName() { 
//			String lastName[] = {"smith", "williams", "johnson", "jones"};
//			Random randomLName = new Random(); 
//			int select = randomLName.nextInt(lastName.length);
//			System.out.println("Randomly selected last name: " + lastName[select]);
//			return lastName[select];  
//		}
		
			public void dirSearch() throws Exception {
			waitForElement(By.id("form-field-0")).clear();
			waitForElement(By.id("form-field-0")).sendKeys(randomfistName());
			waitForElement(By.id("form-field-4")).clear();
			waitForElement(By.id("form-field-4")).sendKeys(randomLastName());
			waitForElement(By.id("dir-run-advanced-search")).click();
			clickResult(); 
		}
		
		//Randomly select from element list
		public static int RandomSelectInt(List<WebElement> waitForElements ){
		    int options = waitForElements.size();
		    Random random = new Random();
		    //System.out.println("index term " + random.nextInt(options));
		    return random.nextInt(options);
		    
		}
		
		// checks to see if an element is displayed or not. 
		public static boolean isElementPresent(By locator) throws Exception, IOException,SQLException {
			try {
		    	waitForElement(locator).isDisplayed();
		    	//System.out.println("element exists");
		    	System.out.println("element exists");
		    	return true;
		    }catch (Throwable t ){
		    	String s = locator.toString();
		    	System.out.println(s + " element does not exists"); 	
				return false;
			}
		}
		
		//checks to see if edit button is visible or not. 
		public boolean isEditButPresent() throws Exception {
			try {
		    	if (isElementPresent(By.xpath("(//div[contains(@class, 'hidden')])[2]"))) { 
					System.out.println("Edit option is disable");
				} else {
					waitForElement(By.id("edit-profile")).click();
					System.out.println("clicking on edit profile");
				}
		    }catch (Throwable t){
		    	System.out.println("Cannot find Edit Button");
				return false;
			}
		    return true;
		}
		
		/**
		 * from http://santoshsarmajv.blogspot.in/ <br>
		 * use this website to verify if date are output correctly http://www.timeanddate.com/date/dateadd.html <br>
		 * 
		 * @param period - The date can be changed by  inserting navigate or positive number. Negative number = past/later date
		 * and positive number = future date. if today date is 11/19/2013, calling getDate(5, "MM/dd/yyyy") 
		 * is 5 days later from today so it will get 11/24/2013. enter -5 will get past date, so it will be 11/14/2013 <br>
		 * 
		 * @param format - date format <br>
		 * 
		 * @return 
		 * getDate(period, format); 
		 * 
		 */
		public String getDate(int period,String format){
		     Calendar currentDate = Calendar.getInstance();
		     SimpleDateFormat formatter= new SimpleDateFormat(format);
		     currentDate.add(Calendar.DAY_OF_MONTH, period);
		     String date = formatter.format(currentDate.getTime());
		     return date;
		}
	
		//finds internal links in an article, clicks on each, and returns to original article after each click
		public void articleSubLinks( ) throws Exception {
			List<WebElement> dirSubLink = SeleniumDriver.getDriver().findElements(By.xpath("//div[contains(@id,'article-description')]/p/a")); //list of people profile 
			//dirSubLink = driver.findElements(By.xpath("//div[contains(@id,'article-description')]/a"));
			List<String> linkTitles = new ArrayList<String>();
			for (WebElement eachlink : dirSubLink){ // get links text and save into list 
				linkTitles.add(eachlink.getText()); 
			}
			for (String eachLink : linkTitles) { // 
				List<WebElement> templinks = SeleniumDriver.getDriver().findElements(By.tagName("a")); // 
				for (WebElement e : templinks) {
					if (eachLink.equals(e.getText())) {
						if (e.isDisplayed() && e.isEnabled()) {
							System.out.println("Found a link. Clicking " + e.getText());						
							e.click();
							Thread.sleep(5000);	
							SeleniumDriver.getDriver().navigate().back();
							break;
						}
					}
				}
			}
		}
		
		/* A method that emails  when an error occurs in performing
		 * Copied from Alan's arm test
		 */
/*		protected static boolean email(String case_content){
			// SUBSTITUTE YOUR EMAIL ADDRESSES HERE!!!
	        String from = "hipatel@lbpc.com";
	        // SUBSTITUTE YOUR ISP'S MAIL SERVER HERE!!!
	        String host = "lb-sunsmtp";
	        // Create properties, get Session
	        Properties props = new Properties();
	        // If using static Transport.send(),
	        // need to specify which host to send it to
	        props.put("mail.smtp.host", host);
	        // To see what is going on behind the scene
	        //props.put("mail.debug", "true");
	        Session session = Session.getInstance(props);
	        try {
	            //Instantiate a message
	            Message msg = new MimeMessage(session);
	            
	            //Set message attributes
	            msg.setFrom(new InternetAddress(from));
	           
	            String [] to = {"hipatel@lbpc.com", "patelhiren1590@gmail.com"};
	        	InternetAddress[] address = new InternetAddress[to.length];
	            for (int i = 0; i < to.length; i++){
	            	address [i] = new InternetAddress(to[i]);
	            }
	        	msg.setRecipients(Message.RecipientType.TO, address);
	        	msg.setSubject("Search Term Do Not Match ");
	            
	            msg.setSentDate(new Date());
	            // Set message content
	            msg.setText(case_content);
	            //Send the message
	            Transport.send(msg);
	        }
	        catch (MessagingException mex) {
	            // Prints all nested (chained) exceptions as well
	            mex.printStackTrace();
	        }
	        return true;
		}	*/
		
		/**
		 * 
		 * @param category 	 JVR Search Category i.e Injury/Disease </p>
		 * @param subCategory   JVR Sub Category appears after category is selected i.e Injury/Disease > J-L - 32 </p>
		 * @param selectALL  	Boolean, True means it will select all of the Terms after Category adn SubCateogry are selected. 
		 * 						False means do not select all of the terms. NOTE, if False is selected you have to enter index number </p>
		 * @param index 		uses integer defined by user to randomly selects terms 
		 * @throws Exception
		 */
		
		public void jVRCategories(String category, String subCategory, boolean selectALL,  String oneTerm) throws Exception {
			waitForElement(By.xpath("//a[contains(@id,'add-jv-terms')]")).click(); // open 
//			ArrayList<String> saveSubCategoryList = new ArrayList<String>();  
//			ArrayList<String> saveTermsList = new ArrayList<String>();  
			if (isElementPresent(By.className("modal-content"))){
				 WebElement selectCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-left')]/ul"));
				 List<WebElement> categoryOptions = selectCategory.findElements(By.tagName("li"));
				    for(WebElement option : categoryOptions){
				        if(option.getText().equals(category)) {
				            option.click();
				            break;
				        }
				    }
			    WebElement selectSubCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-mid')]/ul"));
				List<WebElement> categorySuboptions = selectSubCategory.findElements(By.tagName("li"));
//					trying to save the subcategory into list so i can print them of 
//					for (int i = 1; i == categorySuboptions.size();  i++ ){
//						String test = categorySuboptions.get(i).getText();
//						saveSubCategoryList.add(test);
//					}
//					System.out.println("Sub Cateogry List: " + saveSubCategoryList);
			    	for(WebElement option : categorySuboptions){
			    		if(option.getText().equals(subCategory)) {
			    			option.click();
			    			break;
			    		}
			    	}
			    	
			    	if (selectALL) {
			    		//click select all 
			    		waitForElement(By.id("jv-term-select-select-all")).click();
			    	} else {
			    			WebElement selectTerms = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-right')]/ul"));
							List<WebElement> terms = selectTerms.findElements(By.tagName("li"));
							for(WebElement option : terms){
						        if(option.getText().equals(oneTerm)) {
						            option.click();
						            break;
						        }
							}
			    		
			    	}
			}
			waitForElement(By.id("form-button-Save")).click();
		}
		
		//checks to see if element is clickable
		public static boolean isElementEnable(By locator) throws Exception, IOException,SQLException { 
			try {
		    	waitForElement(locator).isEnabled();
		    	//System.out.println("element exists");
		    	System.out.println("element exists");
		    	return true;
		    }catch (Throwable t ){
		    	System.out.println("element does not exists");  	
		    	return false;
			}
		}
		
		
		/**
		 * 
		 * @param category
		 * @param selectALL
		 * @param subCategory if selectall-false then subCategorgy must be defined, else null
		 * @throws Exception
		 */
		public void selectPraticeArea(String category, boolean selectALL, String subCategory ) throws Exception {
//			ArrayList<String> saveSubCategoryList = new ArrayList<String>();  
//			ArrayList<String> saveTermsList = new ArrayList<String>();  
			if (SeleniumDriver.getDriver().findElement(By.className("modal-content")).isDisplayed()){
				 WebElement selectCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'aop-edit-left')]/ul"));
				 List<WebElement> categoryOptions = selectCategory.findElements(By.tagName("li"));
				    for(WebElement option : categoryOptions){
				        if(option.getText().equals(category)) {
				            option.click();
				            break;
				        }
				    }
					if (selectALL) {
			    		//click select all 
			    		SeleniumDriver.getDriver().findElement(By.xpath("(//div[contains(@class, 'aop-edit-label')]/a)[1]")).click();
			    	} else {
					    WebElement selectSubCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'aop-edit-mid')]/ul"));
						List<WebElement> categorySuboptions = selectSubCategory.findElements(By.tagName("li"));
			//				trying to save the subcategory into list so i can print them of 
			//				for (int i = 1; i == categorySuboptions.size();  i++ ){
			//					String test = categorySuboptions.get(i).getText();
			//					saveSubCategoryList.add(test);
			//				}
			//				System.out.println("Sub Cateogry List: " + saveSubCategoryList);
					    	for(WebElement option : categorySuboptions){
					    		if(option.getText().equals(subCategory)) {
					    			option.click();
					    			break;
					    		}
					    	}
					    	
			    	}
			}
			SeleniumDriver.getDriver().findElement(By.id("form-button-Save")).click();
		}
		
		public void selectTypeAheadValue(WebElement locator, String text) {
			WebElement getTypeAheadField = locator; 
			getTypeAheadField.clear();
			getTypeAheadField.sendKeys(text);
			forceWait(1);
			
			WebElement getActiveList = SeleniumDriver.getDriver().findElement(By.className("yui3-aclist-list")); 
			List<WebElement> select = getActiveList.findElements(By.tagName("li")); 
				for (WebElement option : select){
					if (option.getText().contains(text)){
						option.click();
						break;
					}
				}	
		}
		
		public void selectStreamingContainer(WebElement locator, boolean chicagoDaily, boolean chicagoLawyer, boolean clickMore){
			WebElement steramingCOntainer = locator; 
			
		}
		
		public void selectAndClickResult(String clickResultText) {
			WebElement searchRow = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@class,'search-row row-detail')]"));
			WebElement test2 = searchRow.findElement(By.className("search-detail-container")); 
			List<WebElement> clickName = test2.findElements(By.xpath("//a[contains(@class,'search-click-through')]"));
			for (WebElement option : clickName){
				if (option.getText().contains(clickResultText)){
					option.click();
					break;
				}
			}	
			
		}
		
		public void profileLinks( ) throws Exception {
			WebElement findLinks = waitForElement(By.id("profile-links"));
			WebElement profileLinks = findLinks.findElement(By.tagName("ul"));
			List<WebElement> activeLinks = profileLinks.findElements(By.className("clickable-nav"));
			List<String> linkTitles = new ArrayList<String>();
			for (WebElement clickActiveLink : activeLinks ){
				linkTitles.add(clickActiveLink.getText());
			}
			
			for (String activeLink : linkTitles){
				List<WebElement> tempLinks = SeleniumDriver.getDriver().findElements(By.className("clickable-nav"));
				 
				for (WebElement e : tempLinks){
					if (activeLink.equals(e.getText())) {
						if(e.isDisplayed() && e.isEnabled()){
							System.out.println("Found an active links... Clicking " + e.getText());
							e.click();
							forceWait(5);
							break;
						}
					}
				}
			}
		}
		
		public void tfTest() throws Exception{
		//	String result = null;
			if(condition1){System.out.println("Lawyerport conditional test: Pass");
			}else{System.err.println("Lawyerport conditional test: Fail");}
			
//			if(result.contains("Pass"))
//				printColor("Lawyerport conditional test" + ": "+result, 34);
//			else if(result.contains("Fail"))
//				System.err.println("Lawyerport conditional test" + ": "+result);
//			else
//				System.err.println("Error in Test");
//			try {result.contains("Pass");
//				System.out.println("Lawyerport conditional test" + ": "+result);
//			} catch (Throwable t){
//				if(result.contains("Fail"))
//				System.err.println("Lawyerport conditional test" + ": "+result);
//				else System.out.println("Error in Test");
//			}
			System.out.println("------------------");

		}
		
		public void tfTest2() throws Exception{
			if(condition2){System.out.println("Lawyerport conditional test: Pass");
			}else{System.err.println("Lawyerport conditional test: Fail");}
			
//			String result = null;
//			if(condition2){result="Pass";}else{result="Fail";}
//			
//			if(result.contains("Pass"))
//				printColor("Lawyerport conditional test" + ": "+result,34);
//			else if(result.contains("Fail"))
//				System.err.println("Lawyerport conditional test" + ": "+result);
//			else
//				System.err.println("Error in Test");
			System.out.println("------------------");

		}
		
		public void tfTest3() throws Exception{
			if(condition1 && condition2){System.out.println("Lawyerport conditional test: Pass");
			}else{System.err.println("Lawyerport conditional test: Fail");}//{result="Pass";}else{result="Fail";}
			
//			if(result.contains("Pass"))
//				printColor("Lawyerport conditional test" + ": "+result,34);
//			else if(result.contains("Fail"))
//				System.err.println("Lawyerport conditional test" + ": "+result);
//			else
//				System.err.println("Error in Test");
			System.out.println("------------------");

		}
		
		
		//Returns a random name from a list of common first names
		public String randomName(){
			
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("John");
			list.add("William");
			list.add("Benjamin");
			list.add("Robert");
			list.add("David");
			list.add("Noah");
			list.add("Liam");
			list.add("Ryan");
			list.add("Bryan");
			list.add("Ethan");
			list.add("Eric");
			list.add("Alexander");
			list.add("Jason");
			list.add("Joshua");
			list.add("Neil");
			list.add("Michael");
			list.add("Alejandro");
			list.add("Dylan");
			list.add("James");
			list.add("Jose");
			list.add("Javiar");
			list.add("James");
			list.add("Matthew");
			list.add("Muhammad");
			list.add("Martin");
			list.add("Joseph");
			list.add("Richard");
			list.add("Charles");
			list.add("Thomas");
			list.add("Nicholas");
			list.add("Jack");
			list.add("Sebastian");
			list.add("Mary");
			list.add("Patricia");
			list.add("Linda");
			list.add("Barbara");
			list.add("Elizabeth");
			list.add("Jennifer");
			list.add("Sophia");
			list.add("Sarah");
			list.add("Emily");
			list.add("Olivia");
			list.add("Emma");
			list.add("Miriam");
			list.add("Maria");
			list.add("Margaret");
			list.add("Dorothy");
			list.add("Rachel");
			list.add("Ashley");
			list.add("Julia");
			list.add("Angela");
			list.add("Zoe");
			list.add("Mia");
			list.add("Isaac");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Name: " + name);
			return name;
		}

		//Returns a random name from a list of common surnames 
		public String randomLastName(){
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("Jones");
			list.add("Smith");
			list.add("Williams");
			list.add("Stewart");
			list.add("Martinson");
			list.add("Cohen");
			list.add("Levy");
			list.add("Hogan");
			list.add("Patel");
			list.add("Kennedy");
			list.add("Davis");
			list.add("Miller");
			list.add("Taylor");
			list.add("Anderson");
			list.add("White");
			list.add("Jackson");
			list.add("Garcia");
			list.add("Martinez");
			list.add("Lee");
			list.add("Lewis");
			list.add("Walker");
			list.add("Hall");
			list.add("Allen");
			list.add("Young");
			list.add("King");
			list.add("Lopez");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Last Name: " + name);
			return name;
		}
		
		public void write(String s){
			System.out.println(s);
		}
		
//		public String color(String s, int i){
//			s = "\u001b[1;"+ i+"m"+  s + "\u001b[0m";
//			return s;
//			
//		}
	
}


	
		
	
		
		
