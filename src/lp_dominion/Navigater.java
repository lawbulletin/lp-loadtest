package lp_dominion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;

import seleniumWebDriver.SeleniumDriver;
import lp_dominion.AbstractSprint;


public class Navigater extends AbstractSprint{
	
	/**
	 * 
	 * @return Integer equal to the total number of results found in a Court Dockets search
	 * @throws Exception thrown if search attempted on some page other than Court Docket search results page
	 */
	public Integer DocketSearchCount() throws Exception{
		Integer resultNumber = null;
		String results = "";
		String temp;
		try{
		results = waitForElement(By.id("resultCount")).getText();
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) != null){
			temp = results.substring(26);
			temp = temp.trim();
			resultNumber = Integer.parseInt(temp);
		}
		} catch (Throwable t){
			if(waitForElement(By.xpath("//div[contains (@id, 'subTabContent2')]")).getText().contains("No results were found")){
				resultNumber=0;}
				else if(!waitForElement(By.xpath("//div[contains (@id, 'subTabContent2')]")).getText().contains("No results were found")){
				System.err.println("DocketSearchCount method can only be used following a legitimate docket search");}
		}
		
		System.out.println(resultNumber);
		
		return resultNumber;
	}
	
	/**
	 * 
	 * @param courtDate - Must be a date in the format MM/dd/yyyy
	 * @return - long epoch (epoch -> courtDate represented as a Epoch & Unix Time-stamp)
	 * @throws Exception
	 */
	public static long DateToEpoch(String courtDate) throws Exception{
		long epoch = 0;
		Date date;
		//System.out.println("Date");
		DateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		date = format.parse(courtDate);
		
		Calendar myDate = new GregorianCalendar();
		myDate.setTime(date);
//		System.out.println(myDate.get(Calendar.DATE));
//		System.out.println(myDate.get(Calendar.DAY_OF_YEAR));
//		System.out.println(myDate.get(Calendar.WEEK_OF_YEAR));
//		System.out.println(myDate.get(Calendar.YEAR));
//		System.out.println(myDate.get(Calendar.MONTH));
//		System.out.println("____________________");
		
		epoch = myDate.getTimeInMillis();
		epoch = epoch/1000;
		
		System.out.println(epoch);
	
		return epoch;
		
	}
	
	
		
	
	/**
	 * FormFiller(String, String) --> WebDriver
	 * 
	 * FormFiller takes two strings, the first which identifies an input field, 
	 * the second the string to be input, and returns an updated WebDriver
	 * 
	 * Only works for fields that follow convention of giving each input field an 
	 * id beginning with "form-field-" followed by a number. 
	 **/
	public WebDriver FormFiller(String fnumb, String keys){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id("form-field-"+fnumb)));
		WebElement field = SeleniumDriver.getDriver().findElement(By.id("form-field-"+fnumb));
			field.sendKeys(keys);

		return SeleniumDriver.getDriver();
	}
	
	/**
	 * @param fname = webelement's id (must be a field that accepts a string as an input)
	 * @param keys = keys to be input into webelement field
	 * @Return e = WebElement with value(equal to keys) input into field
	 * 
	 **/
	public WebElement FieldFiller(String fname, String keys){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id(fname)));
		WebElement field = SeleniumDriver.getDriver().findElement(By.id(fname));
			field.sendKeys(keys);
			e =field;
			
			return e;
	}
	
	/**
	 * @param driver
	 * @param elementID
	 * @return WebElement
	 */
	
	public WebElement LPElement( String elementID){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id(elementID)));
		e = SeleniumDriver.getDriver().findElement(By.id(elementID));
		
		return e;
	}
	
	
	
	public WebDriver Directory(){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id("navDir")));
		SeleniumDriver.getDriver().findElement(By.id("navDir")).click();
		
		return SeleniumDriver.getDriver();
	}
	
	public static WebDriver Research(){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id("navRes")));
		SeleniumDriver.getDriver().findElement(By.id("navRes")).click();
		
		return SeleniumDriver.getDriver();
	}
	
	public static WebDriver News(){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id("navNews")));
		SeleniumDriver.getDriver().findElement(By.id("navNews")).click();
		
		return SeleniumDriver.getDriver();
	}
	
	public static WebDriver MarketPlace(){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id("navVend")));
		SeleniumDriver.getDriver().findElement(By.id("navVend")).click();
		
		return SeleniumDriver.getDriver();
	}
	
	public static WebDriver Events(){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id("navEvents")));
		SeleniumDriver.getDriver().findElement(By.id("navEvents")).click();
		
		return SeleniumDriver.getDriver();
	}
	
//	public void TestLine(){
//		System.out.println("Testing:"+" "+issue);
//		System.out.println("------------------");
//	}
//	
	@Rule
	public ErrorCollector collector;
	
}
