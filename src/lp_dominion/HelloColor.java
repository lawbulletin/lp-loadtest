package lp_dominion;

public class HelloColor {
	public static void main(String [] args) {
        System.out.println("Hello \u001b[1;31mred\u001b[0m world!");
       
        printColor("30",30);
        printColor("31",31);
        printColor("32",32);
        printColor("33",33);
        printColor("34",34);
        printColor("35",35);
        printColor("36",36);
        printColor("37",37);
 
    }
	public static String color(String s, int i){
		s = "\u001b[1;"+ i +"m"+ s + "\u001b[0m";
		return s;
		
	}
	
	public static void printColor(String s, int i){
		System.out.println(color(s,i));
	}
}
