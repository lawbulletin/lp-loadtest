package lp_dominion;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import pagefactorylibrary.CourtDocketResultFactory;
import pagefactorylibrary.LPMainPageFactory;
import pagefactorylibrary.LegalResearchPageFactory;
//import seleniumwebdriver.SeleniumDriver;
import seleniumWebDriver.SeleniumDriver;

/**
 * 
 * @author bgalatzer-levy
 *
 */



public class ReuseableMethod{
	LPMainPageFactory main = PFCaller.lpMainFactory();
	LegalResearchPageFactory legal = PFCaller.lrFactory();
	WebDriverWait tock = AbstractSprint.tock;
	WebElement e = AbstractSprint.e;
	
	/**
	 * @param courtDate - Must be a date in the format MM/dd/yyyy
	 * @return - long epoch (epoch -> courtDate represented as a Epoch & Unix Time-stamp)
	 * @throws Exception
	 */
	public static long DateToEpoch(String courtDate) throws Exception{
		long epoch = 0;
		Date date;
		//System.out.println("date");
		DateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		date = format.parse(courtDate);
		
		Calendar myDate = new GregorianCalendar();
		myDate.setTime(date);		
		epoch = myDate.getTimeInMillis();
		epoch = epoch/1000;
		
		System.out.println(epoch);
	
		return epoch;
	}
	
	
	
	
//	/**
//	 * 
//	 * @param type: 1 -id, 2 -class, 3 -xpath, 4 -name, 5 -css
//	 * 				6 - link text 7 -id or name 8 -partial link text
//	 * 				9 -tag name
//	 * @param locater - PageFactory unique locater
//	 * @param name - pageFactory element name
//	 */
//	public static void PFWriter(int type, String locater, String name){
//		String[] returns = new String[2];
//		String Name = name.substring(0).toUpperCase() + name.substring(1, name.length());
//		String how = "@FindBy(how = How.";
//		String using = ", using = \"" + locater +  "\"); " + "\n  private WebElement " +name +"\"";
//		String get = "public WebElement get";
//		String give = "(){ return ";
//		String dingbat = ";}";
//		String findBy = null;
//		String caller = get + Name + give + name + dingbat;
//		String getter = null;
//		switch(type){
//			case 1: 
//				findBy = "@FindBy(id = \"" + locater + "\"); private WebElement " + name;
//				getter = caller;
//				break;
//			case 2:
//				findBy = how + "CLASS_NAME" + using;
//				getter = caller;
//				break;
//			case 3:
//				findBy = how + "XPATH" +using;
//				getter = caller;
//				break;
//			case 4:
//				findBy = "@FindBy(name = " + locater + "\"); private WebElement " + name;
//				getter = caller;
//				break;
//			case 5:
//				findBy = how + "CSS" +using;
//				getter = caller;
//				break;
//			case 6:
//				findBy = how + "LINK_TEXT"+using;
//				getter = caller;
//				break;
//			case 7:
//				findBy = how + "ID_OR_NAME"+using;
//				getter = caller;
//				break;
//			case 8:
//				findBy = how + "PARTIAL_LINK_TEXT"+using;
//				getter = caller;
//				break;
//			case 9:
//				findBy = how + "TAG_NAME"+using;
//				getter = caller;
//				break;	
//		}
//		returns[0] = findBy;
//		returns[1] = getter;
//		System.out.println();
//
//	}
	
	public void  Directory(){
		main.getDirectoryTab().click();
	}
	
	public void Research(){
		main.getLegalResearchLink().click();
	
	}
	
	public void News(){
		main.getNewsLink().click();
	
	}
	
	public  void MarketPlace(){
		main.getLegalMarketplace().click();
	}
	
	public  void Events(){
		main.getEvents().click();
	}
	
	public void navCourtDocket() {
		main.getLegalResearchLink().click();
	
	}
	
	public void navJVR(){
		main.getLegalResearchLink().click();
		legal.getJVS().click();
	
	}
	
	public void navAppellateSummaries() {
		main.getLegalResearchLink().click();
		legal.getAppSummaries().click();
	}
	
	//waits for an element for to appear on page, the default time is 30 seconds.
	//default time to wait for element is 30 seconds.
	public static WebElement waitForElement(final By Locator) throws Exception {
			 WebDriverWait wait = new WebDriverWait(SeleniumDriver.getDriver(), 30);

		    WebElement function = wait.until(new Function<WebDriver, WebElement>() {
		    	
		    	public WebElement apply(WebDriver driver) {
		            return driver.findElement(Locator);
		        }
		    });

		    return  function;
		}
		
		
	public void clickResult() throws Exception {
			try  { 
	    		WebElement searchRow = waitForElement(By.xpath("//div[contains(@class,'search-row row-detail')]"));
				WebElement test2 = searchRow.findElement(By.className("search-detail-container")); 
				List<WebElement> clickName = test2.findElements(By.xpath("//a[contains(@class,'search-click-through')]"));
				int num = (int)(Math.random() * clickName.size()); 
				Thread.sleep(2000);
				String text = clickName.get(num).getText(); 
				System.out.println("clicking on Serach Result: " + text );
				clickName.get(num).click();
				Thread.sleep(5000);
			}catch (IndexOutOfBoundsException  t ){
				System.out.println("No results found, please refine your search");
			}	 
		}

	
	
	public static Boolean booleanElement(WebElement e){
		Boolean b;
		if(!e.isDisplayed())
			b = false;
		else{b = true;}
		
		return b;
	}

	//Returns a random name from a list of common surnames 
	public String randomLastName(){
		int i;
		String name;
		ArrayList<String> list = new ArrayList<String>();
		list.add("Maduff");
		list.add("Kelly");
		list.add("Mazura");
		list.add("Salpeter");
		list.add("Elbert");
		list.add("Larson");
		list.add("Ashley");
		list.add("Valukas");
		list.add("Romanucci");
		list.add("Howe");
		list.add("Lyerla");
		list.add("Pfaff");
		list.add("Montgomery");
		list.add("Wolf");
		list.add("Webb");
		list.add("Klein");
		list.add("Reidy");
		list.add("Levy");
		list.add("Barry");
		list.add("Brown");
		list.add("Stellato");
		list.add("Masters");
		list.add("McNabola");
		list.add("Manzo");
		list.add("Macey");
		list.add("Schwartz");
		list.add("Buckley");
		list.add("Bartlit");
		list.add("Elden");
		list.add("Hartmann");
		list.add("Adelman");
		list.add("Suskin");
		list.add("Fisher");
		list.add("Eaton");
		list.add("Franczek");
		list.add("Hurst");
		list.add("Sprayregen");
		list.add("Rubin");
		list.add("Stone");
		list.add("Nijman");
		list.add("Berkeley");
		list.add("Perconti");
		list.add("Scanlon");
		list.add("Malkinson");
		list.add("Lopez");
		list.add("Power");
		list.add("Tilson");
		list.add("Bisceglia");
		list.add("Gagliardo");
		list.add("Enright");
		
		
		i = list.size();
		int random = new Random().nextInt(i);
		name = list.get(random);
		System.out.println("Random Last Name: " + name);
		return name;
	}

	//Returns a random name from a list of common first names
	public String randomName(){
		
		int i;
		String name;
		ArrayList<String> list = new ArrayList<String>();
		list.add("Aaron");
		list.add("Adam");
		list.add("Adrianne");
		list.add("Alan");
		list.add("Angela");
		list.add("Anne");
		list.add("Anthony");
		list.add("Anton");
		list.add("Antonio");
		list.add("Arthur");
		list.add("Bradford");
		list.add("Bruce");
		list.add("C");
		list.add("Charles");
		list.add("Dan");
		list.add("Daniel");
		list.add("Daniel");
		list.add("David");
		list.add("David");
		list.add("Deane");
		list.add("Donald");
		list.add("Douglas");
		list.add("Edward");
		list.add("Eric");
		list.add("Esther");
		list.add("Francis");
		list.add("Fred");
		list.add("Gary");
		list.add("H");
		list.add("Howard");
		list.add("Howard");
		list.add("Ian");
		list.add("J");
		list.add("James");
		list.add("James");
		list.add("james");
		list.add("James");
		list.add("Jeffrey");
		list.add("Jennifer");
		list.add("Jill");
		list.add("John");
		list.add("John");
		list.add("John");
		list.add("Jose");
		list.add("Joseph");
		list.add("Joseph");
		list.add("Joseph");
		list.add("Joseph");
		list.add("Karen");
//		list.add("Ashley");
//		list.add("Julia");
//		list.add("Angela");
//		list.add("Zoe");
//		list.add("Mia");
//		list.add("Isaac");
		
		i = list.size();
		int random = new Random().nextInt(i);
		name = list.get(random);
		System.out.println("Random Name: " + name);
		return name;
	}

	public String randomOrg(){
		
		int i;
		String org;
		ArrayList<String> list = new ArrayList<String>();
		list.add("Jenner & Block");
		list.add("100th District");
		list.add("10th Judicial Circuit");
		list.add("A B Data");
		list.add("Wisconsin Department of Revenue");
		list.add(" O'Connor & O'Connor PC");
		list.add("Catholic Charities");
		list.add("Loyola University Chicago");
	
		i = list.size();
		int random = new Random().nextInt(i);
		org = list.get(random);
		System.out.println("Random Organization: " + org);
		return org;
	}
	
	public void selectStreamingContainer(WebElement locator, boolean chicagoDaily, boolean chicagoLawyer, boolean clickMore){
		WebElement steramingCOntainer = locator; 
		
	}

	/**
			 * 
			 * @param category
			 * @param selectALL
			 * @param subCategory if selectall-false then subCategorgy must be defined, else null
			 * @throws Exception
			 */
	public void selectPraticeArea(String category, boolean selectALL, String subCategory ) throws Exception {
	//			ArrayList<String> saveSubCategoryList = new ArrayList<String>();  
	//			ArrayList<String> saveTermsList = new ArrayList<String>();  
				if (SeleniumDriver.getDriver().findElement(By.className("modal-content")).isDisplayed()){
					 WebElement selectCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'aop-edit-left')]/ul"));
					 List<WebElement> categoryOptions = selectCategory.findElements(By.tagName("li"));
					    for(WebElement option : categoryOptions){
					        if(option.getText().equals(category)) {
					            option.click();
					            break;
					        }
					    }
						if (selectALL) {
				    		//click select all 
				    		SeleniumDriver.getDriver().findElement(By.xpath("(//div[contains(@class, 'aop-edit-label')]/a)[1]")).click();
				    	} else {
						    WebElement selectSubCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'aop-edit-mid')]/ul"));
							List<WebElement> categorySuboptions = selectSubCategory.findElements(By.tagName("li"));
				//				trying to save the subcategory into list so i can print them of 
				//				for (int i = 1; i == categorySuboptions.size();  i++ ){
				//					String test = categorySuboptions.get(i).getText();
				//					saveSubCategoryList.add(test);
				//				}
				//				System.out.println("Sub Cateogry List: " + saveSubCategoryList);
						    	for(WebElement option : categorySuboptions){
						    		if(option.getText().equals(subCategory)) {
						    			option.click();
						    			break;
						    		}
						    	}
						    	
				    	}
				}
				SeleniumDriver.getDriver().findElement(By.id("form-button-Save")).click();
			}

	public void selectTypeAheadValue(WebElement locator, String text) {
		WebElement getTypeAheadField = locator; 
		getTypeAheadField.clear();
		getTypeAheadField.sendKeys(text);
		forceWait(2);
		
		WebElement getActiveList = SeleniumDriver.getDriver().findElement(By.className("yui3-aclist-list")); 
		List<WebElement> select = getActiveList.findElements(By.tagName("li")); 
			for (WebElement option : select){
				if (option.getText().contains(text)){
					option.click();
					break;
				}
			}	
	}

	
	
	public void selectFromTypeAheadValues(WebElement locator, String text) {
		WebElement getTypeAheadField = locator; 
		getTypeAheadField.clear();
		getTypeAheadField.sendKeys(text);
		forceWait(2);
		
		List<WebElement> select = SeleniumDriver.getDriver().findElements(By.className("yui3-aclist-item")); 
		 for (WebElement option : select){
				if (option.getText().contains(text)){
					option.click();
					break;
				}
			}	
	}
	
	/* A method that emails  when an error occurs in performing
			 * Copied from Alan's arm test
			 */
	/*		protected static boolean email(String case_content){
				// SUBSTITUTE YOUR EMAIL ADDRESSES HERE!!!
		        String from = "hipatel@lbpc.com";
		        // SUBSTITUTE YOUR ISP'S MAIL SERVER HERE!!!
		        String host = "lb-sunsmtp";
		        // Create properties, get Session
		        Properties props = new Properties();
		        // If using static Transport.send(),
		        // need to specify which host to send it to
		        props.put("mail.smtp.host", host);
		        // To see what is going on behind the scene
		        //props.put("mail.debug", "true");
		        Session session = Session.getInstance(props);
		        try {
		            //Instantiate a message
		            Message msg = new MimeMessage(session);
		            
		            //Set message attributes
		            msg.setFrom(new InternetAddress(from));
		           
		            String [] to = {"hipatel@lbpc.com", "patelhiren1590@gmail.com"};
		        	InternetAddress[] address = new InternetAddress[to.length];
		            for (int i = 0; i < to.length; i++){
		            	address [i] = new InternetAddress(to[i]);
		            }
		        	msg.setRecipients(Message.RecipientType.TO, address);
		        	msg.setSubject("Search Term Do Not Match ");
		            
		            msg.setSentDate(new Date());
		            // Set message content
		            msg.setText(case_content);
		            //Send the message
		            Transport.send(msg);
		        }
		        catch (MessagingException mex) {
		            // Prints all nested (chained) exceptions as well
		            mex.printStackTrace();
		        }
		        return true;
			}	*/
			
			/**
			 * 
			 * @param category 	 JVR Search Category i.e Injury/Disease </p>
			 * @param subCategory   JVR Sub Category appears after category is selected i.e Injury/Disease > J-L - 32 </p>
			 * @param selectALL  	Boolean, True means it will select all of the Terms after Category adn SubCateogry are selected. 
			 * 						False means do not select all of the terms. NOTE, if False is selected you have to enter index number </p>
			 * @param index 		uses integer defined by user to randomly selects terms 
			 * @throws Exception
			 */
			
			public void jVRCategories(String category, String subCategory, boolean selectALL,  String oneTerm) throws Exception {
				waitForElement(By.xpath("//a[contains(@id,'add-jv-terms')]")).click(); // open 
	//			ArrayList<String> saveSubCategoryList = new ArrayList<String>();  
	//			ArrayList<String> saveTermsList = new ArrayList<String>();  
				if (isElementPresent(By.className("modal-content"))){
					 WebElement selectCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-left')]/ul"));
					 List<WebElement> categoryOptions = selectCategory.findElements(By.tagName("li"));
					    for(WebElement option : categoryOptions){
					        if(option.getText().equals(category)) {
					            option.click();
					            break;
					        }
					    }
				    WebElement selectSubCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-mid')]/ul"));
					List<WebElement> categorySuboptions = selectSubCategory.findElements(By.tagName("li"));
	//					trying to save the subcategory into list so i can print them of 
	//					for (int i = 1; i == categorySuboptions.size();  i++ ){
	//						String test = categorySuboptions.get(i).getText();
	//						saveSubCategoryList.add(test);
	//					}
	//					System.out.println("Sub Cateogry List: " + saveSubCategoryList);
				    	for(WebElement option : categorySuboptions){
				    		if(option.getText().equals(subCategory)) {
				    			option.click();
				    			break;
				    		}
				    	}
				    	
				    	if (selectALL) {
				    		//click select all 
				    		waitForElement(By.id("jv-term-select-select-all")).click();
				    	} else {
				    			WebElement selectTerms = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-right')]/ul"));
								List<WebElement> terms = selectTerms.findElements(By.tagName("li"));
								for(WebElement option : terms){
							        if(option.getText().equals(oneTerm)) {
							            option.click();
							            break;
							        }
								}
				    		
				    	}
				}
				waitForElement(By.id("form-button-Save")).click();
			}

	/* A method that emails  when an error occurs in performing
					 * Copied from Alan's arm test
					 */
			/*		protected static boolean email(String case_content){
						// SUBSTITUTE YOUR EMAIL ADDRESSES HERE!!!
				        String from = "hipatel@lbpc.com";
				        // SUBSTITUTE YOUR ISP'S MAIL SERVER HERE!!!
				        String host = "lb-sunsmtp";
				        // Create properties, get Session
				        Properties props = new Properties();
				        // If using static Transport.send(),
				        // need to specify which host to send it to
				        props.put("mail.smtp.host", host);
				        // To see what is going on behind the scene
				        //props.put("mail.debug", "true");
				        Session session = Session.getInstance(props);
				        try {
				            //Instantiate a message
				            Message msg = new MimeMessage(session);
				            
				            //Set message attributes
				            msg.setFrom(new InternetAddress(from));
				           
				            String [] to = {"hipatel@lbpc.com", "patelhiren1590@gmail.com"};
				        	InternetAddress[] address = new InternetAddress[to.length];
				            for (int i = 0; i < to.length; i++){
				            	address [i] = new InternetAddress(to[i]);
				            }
				        	msg.setRecipients(Message.RecipientType.TO, address);
				        	msg.setSubject("Search Term Do Not Match ");
				            
				            msg.setSentDate(new Date());
				            // Set message content
				            msg.setText(case_content);
				            //Send the message
				            Transport.send(msg);
				        }
				        catch (MessagingException mex) {
				            // Prints all nested (chained) exceptions as well
				            mex.printStackTrace();
				        }
				        return true;
					}	*/
					
					// using random function to select random name from the lastName Array 
			//		public static String randomLastName() { 
			//			String lastName[] = {"smith", "williams", "johnson", "jones"};
			//			Random randomLName = new Random(); 
			//			int select = randomLName.nextInt(lastName.length);
			//			System.out.println("Randomly selected last name: " + lastName[select]);
			//			return lastName[select];  
			//		}
					
	public void dirSearch() throws Exception {
						waitForElement(By.id("form-field-0")).clear();
						waitForElement(By.id("form-field-0")).sendKeys(randomfistName());
						waitForElement(By.id("form-field-4")).clear();
						waitForElement(By.id("form-field-4")).sendKeys(randomLastName());
						waitForElement(By.id("dir-run-advanced-search")).click();
						clickResult(); 
					}

	public void docketDateSearch() throws Exception {
							waitForElement(By.id("navRes")).click();
							waitForElement(By.xpath("//a[contains(.,'Court Dockets')]")).click();
							
							System.out.println("Court docket date search");
							waitForElement(By.id("form-field-6")).sendKeys(randomLastName()); // Defendant
						
							waitForElement(By.id("form-field-1")).sendKeys(getDate(-100, "MM/dd/yyyy")); // start date
							Thread.sleep(2000); // because by default end date will get outputed  
							waitForElement(By.id("form-field-2")).clear(); // end date 
							waitForElement(By.id("form-field-2")).sendKeys(getDate(5, "MM/dd/yyyy")); // end date 	
							Thread.sleep(10000);
							waitForElement(By.id("court-dockets-run-advanced-search")).click();
							//clickDocketResult();
						}

	public Integer docketSearchCount() throws Exception{
		CourtDocketResultFactory cdResults = PFCaller.cdResults();
				Integer resultNumber = null;
				String results = "";
				String temp;
				try{
				
				if(cdResults.getResultCount().isDisplayed()){
					results = AbstractSprint.getString(cdResults.getResultCount());
					temp = results.substring(26);
					temp = temp.trim();
					resultNumber = Integer.parseInt(temp);
				}
				} catch (Throwable t){
					if(cdResults.getNoResultsBar().isDisplayed()){
						resultNumber=0;}
						
				}
				
				System.out.println(resultNumber);
				
				return resultNumber;
			}

	public void formFiller(WebElement e, String keys){
				e.sendKeys(keys);
			}

			/**
			 * from http://santoshsarmajv.blogspot.in/ <br>
			 * use this website to verify if date are output correctly http://www.timeanddate.com/date/dateadd.html <br>
			 * 
			 * @param period - The date can be changed by  inserting navigate or positive number. Negative number = past/later date
			 * and positive number = future date. if today date is 11/19/2013, calling getDate(5, "MM/dd/yyyy") 
			 * is 5 days later from today so it will get 11/24/2013. enter -5 will get past date, so it will be 11/14/2013 <br>
			 * 
			 * @param format - date format <br>
			 * 
			 * @return 
			 * getDate(period, format); 
			 * 
			 */
			public String getDate(int period,String format){
			     Calendar currentDate = Calendar.getInstance();
			     SimpleDateFormat formatter= new SimpleDateFormat(format);
			     currentDate.add(Calendar.DAY_OF_MONTH, period);
			     String date = formatter.format(currentDate.getTime());
			     return date;
			}

			//checks to see if edit button is visible or not. 
			public boolean isEditButPresent() throws Exception {
				try {
			    	if (isElementPresent(By.xpath("(//div[contains(@class, 'hidden')])[2]"))) { 
						System.out.println("Edit option is disable");
					} else {
						waitForElement(By.id("edit-profile")).click();
						System.out.println("clicking on edit profile");
					}
			    }catch (Throwable t){
			    	System.out.println("Cannot find Edit Button");
					return false;
				}
			    return true;
			}

			public void profileLinks( ) throws Exception {
				WebElement findLinks = waitForElement(By.id("profile-links"));
				WebElement profileLinks = findLinks.findElement(By.tagName("ul"));
				List<WebElement> activeLinks = profileLinks.findElements(By.className("clickable-nav"));
				List<String> linkTitles = new ArrayList<String>();
				for (WebElement clickActiveLink : activeLinks ){
					linkTitles.add(clickActiveLink.getText());
				}
				
				for (String activeLink : linkTitles){
					List<WebElement> tempLinks = SeleniumDriver.getDriver().findElements(By.className("clickable-nav"));
					 
					for (WebElement e : tempLinks){
						if (activeLink.equals(e.getText())) {
							if(e.isDisplayed() && e.isEnabled()){
								System.out.println("Found an active links... Clicking " + e.getText());
								e.click();
								forceWait(5);
								break;
							}
						}
					}
				}
			}

			/* A method that emails  when an error occurs in performing
					 * Copied from Alan's arm test
					 */
			/*		protected static boolean email(String case_content){
						// SUBSTITUTE YOUR EMAIL ADDRESSES HERE!!!
				        String from = "hipatel@lbpc.com";
				        // SUBSTITUTE YOUR ISP'S MAIL SERVER HERE!!!
				        String host = "lb-sunsmtp";
				        // Create properties, get Session
				        Properties props = new Properties();
				        // If using static Transport.send(),
				        // need to specify which host to send it to
				        props.put("mail.smtp.host", host);
				        // To see what is going on behind the scene
				        //props.put("mail.debug", "true");
				        Session session = Session.getInstance(props);
				        try {
				            //Instantiate a message
				            Message msg = new MimeMessage(session);
				            
				            //Set message attributes
				            msg.setFrom(new InternetAddress(from));
				           
				            String [] to = {"hipatel@lbpc.com", "patelhiren1590@gmail.com"};
				        	InternetAddress[] address = new InternetAddress[to.length];
				            for (int i = 0; i < to.length; i++){
				            	address [i] = new InternetAddress(to[i]);
				            }
				        	msg.setRecipients(Message.RecipientType.TO, address);
				        	msg.setSubject("Search Term Do Not Match ");
				            
				            msg.setSentDate(new Date());
				            // Set message content
				            msg.setText(case_content);
				            //Send the message
				            Transport.send(msg);
				        }
				        catch (MessagingException mex) {
				            // Prints all nested (chained) exceptions as well
				            mex.printStackTrace();
				        }
				        return true;
					}	*/
					
	public void selectAndClickResult(String clickResultText) {
			WebElement searchRow = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@class,'search-row row-detail')]"));
			WebElement test2 = searchRow.findElement(By.className("search-detail-container")); 
			List<WebElement> clickName = test2.findElements(By.xpath("//a[contains(@class,'search-click-through')]"));
			for (WebElement option : clickName){
			if (option.getText().contains(clickResultText)){
					option.click();
					break;
							}
						}	
						
					}

			//finds internal links in an article, clicks on each, and returns to original article after each click
	public void articleSubLinks( ) throws Exception {
			List<WebElement> dirSubLink = SeleniumDriver.getDriver().findElements(By.xpath("//div[contains(@id,'article-description')]/p/a")); //list of people profile 
			//dirSubLink = driver.findElements(By.xpath("//div[contains(@id,'article-description')]/a"));
			List<String> linkTitles = new ArrayList<String>();
				for (WebElement eachlink : dirSubLink){ // get links text and save into list 
						linkTitles.add(eachlink.getText()); 
				}
			for (String eachLink : linkTitles) { // 
					List<WebElement> templinks = SeleniumDriver.getDriver().findElements(By.tagName("a")); // 
						for (WebElement e : templinks) {
							if (eachLink.equals(e.getText())) {
							if (e.isDisplayed() && e.isEnabled()) {
									System.out.println("Found a link. Clicking " + e.getText());						
									e.click();
									Thread.sleep(5000);	
									SeleniumDriver.getDriver().navigate().back();
									break;
									}
								}
							}
						}
					}

		

								//Randomly select from element list
	public static int RandomSelectInt(List<WebElement> waitForElements ){
		 int options = waitForElements.size();
		 Random random = new Random();
		 //System.out.println("index term " + random.nextInt(options));
			   return random.nextInt(options);
							    
			}

	

			// checks to see if an element is displayed or not. 
	public static boolean isElementPresent(By locator) throws Exception, IOException,SQLException {
				try {
			    	waitForElement(locator).isDisplayed();
			    	//System.out.println("element exists");
			    
			    	return true;
			    }catch (Throwable t ){
			    	String s = locator.toString();
			    	//System.out.println(s + " element does not exists"); 	
					return false;
				}
	}

	// checks to see if an element is displayed or not. 

	public static boolean isWebElementPresent(WebElement e) throws Exception, IOException,SQLException {
			
		try {if( booleanElement(e));
		    	return true;
		    }catch (Throwable t ){
				return false;
			}
}
					//checks to see if element is clickable
	public static boolean isElementEnabled(By locator) throws Exception, IOException,SQLException { 
						try {
					    	waitForElement(locator).isEnabled();
					    	System.out.println("element exists");
					    	return true;
					    }catch (Throwable t ){
					    	System.out.println("element does not exists");  	
					    	return false;
						}
	}

	public void forceWait(int sleepTime){
	//	System.out.println("human wait of: " + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
	
	// using random function to select random name from the firstName Array
	public static String randomfistName() {
			String firstName[] = {"a", "b", "c", "d", "e"};
				Random randomFName = new Random();
					int select = randomFName.nextInt(firstName.length);
					System.out.println("Randomly selected first name: " + firstName[select]); 
					return firstName[select];  
}
	
	
	public static void clickDocketResult() throws Exception{
		if (isElementPresent(By.className("yui3-datatable-content"))){		
			// click on search results. 
			WebElement caseContainer = waitForElement(By.className("yui3-datatable-content")); // entire data table
			//combines the all of the CATEGORY OF CASE into one list. 
			List<WebElement> caseList =  caseContainer.findElements(By.xpath("//tr/td[1]/div[@class='yui3-datatable-liner']/a"));
			caseList.get(RandomSelectInt(caseList)).click(); 
			
			// each of the cases in court docket have list of sub cases 
			if (isElementPresent(By.id("datatable-casename"))){
				try {
					String caseName = waitForElement(By.id("datatable-casename")).getText(); // name of the case that was selected from above code
					System.out.println("Case Name is: " + caseName); // prints the name of the case name 
					// list of sub cases  
					List<WebElement> chkCaseName = SeleniumDriver.getDriver().findElements(By.xpath("//tr/td[2]/div[@class='yui3-datatable-liner']/a"));
					int chkBoxNum = (int)(Math.random() * chkCaseName.size()) + 1;  
					String subCaseName = chkCaseName.get(chkBoxNum).getText();
					chkCaseName.get(chkBoxNum).click();
					System.out.println("sub case selected: " + subCaseName);
					Thread.sleep(2000);
					waitForElement(By.id("form-button-Okay")).click();
				} catch (IndexOutOfBoundsException ie) {
					System.out.println(ie.toString());
				}
			} 	
		}else {
			isElementPresent(By.xpath("//a[contains(.,'Return to the Search Form')]"));
			System.out.println("No so results were found");
		}
	}

}
