package lp_dominion;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import regressionTest.VerifyBadSignIn;
import regressionTest.VerifyClientCodeModel;
import regressionTest.VerifyCourtDockets;
import regressionTest.VerifyCourtRules;
import regressionTest.VerifyHomePage;
import regressionTest.VerifyJuryVerdicts;
import regressionTest.VerifyLandingPage;
import regressionTest.VerifyLogin;
import regressionTest.VerifyLogout;
import regressionTest.VerifyNews;
import regressionTest.VerifyQuickSearch;
import regressionTest.VerifySignInPage;


@RunWith(Suite.class)
@Suite.SuiteClasses({
//	VerifyHomePage.class,
	VerifyLandingPage.class, 
	VerifySignInPage.class, 
	VerifyBadSignIn.class,
	VerifyLogin.class,
	VerifyClientCodeModel.class,
	VerifyQuickSearch.class,
	VerifyCourtDockets.class,
	VerifyCourtRules.class,
	VerifyHomePage.class,
	VerifyJuryVerdicts.class,
	VerifyNews.class,
	VerifyLogout.class
	
	})

public class FullRegressionSuite {
	
	
}
