package lp_dominion;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Navigatoin extends AbstractSprint {
	

	@FindBy (how = How.XPATH, using = "//li[@id='navDir']")
	private WebElement getDirctory; 
	
	@FindBy (how = How.XPATH, using = "/li[contains(@id, 'org')]/a")
	private WebElement getOrg; 
	
	@FindBy(how = How.XPATH, using ="//li[@id='navNews']")
	private WebElement news;
	
	@FindBy (how = How.XPATH, using = "//li[@id='navRes']")
	private WebElement legalResearch; 
	
//	@FindBy(how = How.XPATH, using ="//a[contains(@id, 'subtab-verdicts-settlements')]]")
//	private WebElement jvr;
	
	@FindBy(how = How.XPATH, using ="//a[contains(@id, 'subtab-verdicts-settlements')]]")
	private WebElement appSummaries;
	
	@FindBy(id= "subtab-verdicts-settlements")
	private WebElement jvr;
	
	public WebElement navDIR() {
		return getDirctory;
	}
	
	//bandage
	public WebElement navORG() {
		getDirctory.click();
		forceWait(1);
		return getOrg; 
	}

	
	public WebElement navNews(){
		return news;
	}
	
	/**
	 * nav to court docket page
	 */
	public WebElement navCourtDocket() {
		return legalResearch;
	
	}
	
	public WebElement navJVR(){
		legalResearch.click();
		forceWait(1);
		jvr.click();
		return jvr;
	}
	
	
	public WebElement navAppellateSummaries() {
		legalResearch.click();
		forceWait(2);
		return appSummaries;
	}
	
	
	
}
