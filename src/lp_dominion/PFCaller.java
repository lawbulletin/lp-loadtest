package lp_dominion;

import org.openqa.selenium.support.PageFactory;

import pagefactorylibrary.LegalResearchPageFactory;
import pagefactorylibrary.DirectoryResultFactory;
import pagefactorylibrary.ClientCodeFactory;
import pagefactorylibrary.CourtDocketResultFactory;
import pagefactorylibrary.CourtRulePageFactory;
import pagefactorylibrary.HomePageFactory;
import pagefactorylibrary.LPMainPageFactory;
import pagefactorylibrary.OrgDirectoryPageFactory;
import pagefactorylibrary.CDPageFactory;
import pagefactorylibrary.DirectoryPageFactory;
import pagefactorylibrary.JVRPageFactory;
import pagefactorylibrary.JVRResultFactory;
import pagefactorylibrary.CaseDetailsPageFactory;
import pagefactorylibrary.AppellateSummariesPageFactory;
import pagefactorylibrary.LegalMarketPlacePageFactory;

import pagefactorylibrary.NewsPageFactory;
import pagefactorylibrary.NewsResultFactory;
import pagefactorylibrary.LoginPageFactory;
import pagefactorylibrary.OrgProfilePageFactory;
import pagefactorylibrary.ProfilePageFactory;
import pagefactorylibrary.SearchResultsFactory;
import seleniumWebDriver.SeleniumDriver;


public class PFCaller{
	
	public static  ClientCodeFactory ccFactory(){
		ClientCodeFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), ClientCodeFactory.class);
		return (ClientCodeFactory) pfName;
	}
	
	public static HomePageFactory homeFactory(){
		HomePageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), HomePageFactory.class);
		return (HomePageFactory) pfName;
	}
	
	public static LPMainPageFactory lpMainFactory(){
		LPMainPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LPMainPageFactory.class);
		return (LPMainPageFactory) pfName;
	}
	public static LegalResearchPageFactory lrFactory(){
		LegalResearchPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LegalResearchPageFactory.class);
		return (LegalResearchPageFactory) pfName;
	}
	
	public static CourtRulePageFactory crFactory(){
		CourtRulePageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CourtRulePageFactory.class);
		return (CourtRulePageFactory) pfName;
	}
	
	public static OrgProfilePageFactory orgProfileFactory(){
		OrgProfilePageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), OrgProfilePageFactory.class);
		return (OrgProfilePageFactory) pfName;}
	

	public static CDPageFactory cdFactory(){
		CDPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CDPageFactory.class);
		return (CDPageFactory) pfName;}
	
	public static DirectoryPageFactory directoryFactory(){
		DirectoryPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), DirectoryPageFactory.class);
		return (DirectoryPageFactory) pfName;}
	
	public static DirectoryResultFactory dirResultsFactory(){
		DirectoryResultFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), DirectoryResultFactory.class);
		return pfName;
	}
	
	public static JVRPageFactory jvrFactory(){
		JVRPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), JVRPageFactory.class);
		return (JVRPageFactory) pfName;}
	
	public static JVRResultFactory jvrResult(){
		JVRResultFactory pfname = PageFactory.initElements(SeleniumDriver.getDriver(), JVRResultFactory.class);
		return (JVRResultFactory )pfname;
		
	}
	
	public static OrgDirectoryPageFactory orgFactory(){
		OrgDirectoryPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), OrgDirectoryPageFactory.class);
		return (OrgDirectoryPageFactory) pfName;}
	
	public static CaseDetailsPageFactory caseDetailsFactory(){
		CaseDetailsPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CaseDetailsPageFactory.class);
		return (CaseDetailsPageFactory) pfName;}
	
	public static AppellateSummariesPageFactory appSummaryFactory(){
		AppellateSummariesPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), AppellateSummariesPageFactory.class);
		return (AppellateSummariesPageFactory) pfName;}

	public static LegalMarketPlacePageFactory lmFactory(){
		LegalMarketPlacePageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LegalMarketPlacePageFactory.class);
		return (LegalMarketPlacePageFactory) pfName;}

	

	public static NewsPageFactory newsFactory(){
		NewsPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), NewsPageFactory.class);
		return (NewsPageFactory) pfName;}
	
	public static NewsResultFactory newsResultFactory(){
		
		NewsResultFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), NewsResultFactory.class);
		return (NewsResultFactory) pfName;
	}

	public static LoginPageFactory loginFactory(){
		LoginPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LoginPageFactory.class);
		return pfName;}

	public static ProfilePageFactory profileFactory(){
		ProfilePageFactory pfName =  PageFactory.initElements(SeleniumDriver.getDriver(), ProfilePageFactory.class);
		return pfName;}

	public static SearchResultsFactory searchResultFactoy(){
		SearchResultsFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), SearchResultsFactory.class);
		return pfName;}
	
	public static CourtDocketResultFactory cdResults(){
		
		CourtDocketResultFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CourtDocketResultFactory.class);
		return pfName;
	}

	
}
