package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ClientCodeFactory {

	@FindBy(id = "form-button-Save")
	private WebElement clientCodeOK;
	
	
	@FindBy(id = "client-code-modal-input")
	private WebElement ccModalInput;
	
	@FindBy(id = "client-code-input")
	private WebElement ccInput;
	
	@FindBy(id = "client-code-modal-warning-text")
	private WebElement ccWarning;
	
	@FindBy(id ="form-button-Close") 
	private WebElement formButtonClose;
	
	@FindBy(id = "client-code-modal-title")
	private WebElement ccTitle;
	
	@FindBy(how = How.CLASS_NAME, using ="modal-content-confirmation")
	private WebElement modalConfirmation;
	
	public WebElement getCCCloseButton(){
		return formButtonClose;
	}

	public WebElement getConfirmation(){
		return modalConfirmation;
	}
	
	
	public WebElement getCCTitle(){
		return ccTitle;
	}
	
	public WebElement getCCWarning(){
		return ccWarning;
	}
	
	public WebElement getClientCodeInput(){
		return ccInput;
	}
	
	public WebElement getClientCodeModalInput(){
		return ccModalInput;
	}
	
	public WebElement getClientCodeOK(){
		return clientCodeOK;
	}
	
	
}
