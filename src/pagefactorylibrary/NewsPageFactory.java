package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class NewsPageFactory {
	
	@FindBy(how = How.XPATH, using ="//li[@id='navNews']")
	private WebElement news;
	
	@FindBy (how =How.ID, using ="form-field-0")
	private WebElement keywords; 

	@FindBy (how =How.ID, using ="form-field-1")
	private WebElement publishStart; 
	
	@FindBy (how =How.ID, using ="form-field-2")
	private WebElement PublishEnd; 
	
	@FindBy (how =How.ID, using ="form-field-3")
	private WebElement author; 
	
	@FindBy (how =How.ID, using ="form-field-4")
	private WebElement title; 
	
	@FindBy (how =How.ID, using ="form-field-5")
	private WebElement publication; 
	
	@FindBy(how = How.ID, using = "news-run-advanced-search")
	private WebElement search;
	
	@FindBy(how = How.ID, using = "news-reset-advanced-search-bottom")
	private WebElement reset;
	
	@FindBy(how = How.CSS, using = "div[id='cldb-news-logo']")
	private WebElement cldbLogo;
	
	@FindBy(how = How.CSS, using = "div[class='cms-content'] p a img")
	private WebElement chicagoLawyerLogo;
	
	@FindBy(how = How.CSS, using = "div[id='jv-news-logo']")
	private WebElement juryVerdictLogo;
	
	@FindBy(how = How.CSS, using = "a[id='ct-news']")
	private WebElement courtNewsLogo;
	
	@FindBy(how = How.CSS, using = "div[id='as-news']")
	private WebElement appellateCS;
	
	@FindBy(how = How.CSS, using = "div[id='itn-news']")
	private WebElement itnLink;
	
	@FindBy(how = How.CSS, using = "div[class='cms-content'] p a span:nth-of-type(1) strong")
	private WebElement jobLink;
	
	@FindBy(how = How.CSS, using = "a[id='form-button-Submit']")
	private WebElement continueButton;
	
	@FindBy(how = How.CSS, using = "div[id='leftContent2'] a[id='filter-0-1'] span[class='filter-check-box']")
	private WebElement pastWeekFilter;
	
//	public WebElement navNews(){
//		return news;
//	}
	
	public WebElement getContinueButton(){
		
		return continueButton;
	}
	
	public WebElement getJobLink(){
		
		return jobLink;
	}
	
	public WebElement getItn(){
		
		return itnLink;
	}
	
	public WebElement getAppellateCS(){
		
		return appellateCS;
	}
	
	public WebElement getCourtNewsLogo(){
		
		return courtNewsLogo;
	}
	
	public WebElement getJuryVerdictLogo(){
		
		return juryVerdictLogo;
	}
	
	public WebElement getChicagoLawyerLogo(){
		
		return chicagoLawyerLogo;
	}
	
	public WebElement getCldbLogo(){
		
		return cldbLogo;
	}
	
	public WebElement getKeywords(){
		return keywords;
	}
	
	public WebElement getPublishStart() {
		return publishStart;
	}
	
	public WebElement getPublishEnd() {
		return PublishEnd;
	}
	
	public WebElement getAuthor(){
		return author;
	}
	
	public WebElement getTitle() {
		return title;
	}
	
	public Select getPublication(){
		return new Select(publication);
	}
	
	public WebElement getSearch(){
		return search;
	}
	
	public WebElement getReset(){
		return reset;
	}

}
