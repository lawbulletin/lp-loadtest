package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;

/**
 * @author pthakkar
 *
 */
public class JVRResultFactory {
	
	@FindBy(how = How.CSS, using = "tbody[class='yui3-datatable-data'] tr:nth-of-type(1) td[class='yui3-column-id'] div a")
	private WebElement firstResultCheckBox;
	
	@FindBy(how = How.CSS, using = "a[id='view-selected']")
	private WebElement viewSelectedButton;
	
	@FindBy(how = How.CSS, using = "a[id='back-results']")
	private WebElement searchResultsButton;
	
	@FindBy(how = How.CSS, using ="a[id='form-button-Submit']")
	private WebElement continueButton;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-byLine-0']")
	private WebElement caseNumberCheckText;
	
	@FindBy(how= How.CSS, using = "tbody[class='yui3-datatable-data'] tr:nth-of-type(1) td[class='yui3-column-id'] div a")
	private WebElement firstResultForKeyword;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item--7'] span[class='jv-underline']:nth-of-type(2) span:nth-of-type(1)")
	private WebElement textCheckForFirstResultKeyword8;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span span")
	private WebElement textCheckForFirstResultKeyword2;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span span")
	private WebElement textCheckForFirstResultKeyword3;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span span")
	private WebElement textCheckForFirstResultKeyword4;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span:nth-of-type(1) span span")
	private WebElement textCheckForFirstResultKeyword5;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span:nth-of-type(2)")
	private WebElement textCheck1ForFirstResultKeyword6;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span:nth-of-type(3) span")
	private WebElement textCheck2ForFirstResultKeyword6;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item-0'] span:nth-of-type(2)")
	private WebElement textCheck1ForFirstResultKeyword7;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item--6'] span:nth-of-type(3)")
	private WebElement textCheck2ForFirstResultKeyword7;
	
	@FindBy(how = How.CSS, using = "span[id='all']")
	private WebElement selectAll;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-1'] ul[id='case-article-article-list'] li[id='article-list-item--5'] span[class='jv-underline'] span:nth-of-type(1)")
	private WebElement textCheckSelectALLForKeyword9;
	
	@FindBy(how = How.CSS, using = "div[id='selected-case']")
	private WebElement textSelectedCases;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-pubName-0']")
	private WebElement textCheckForDateRange;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span")
	private WebElement textCheckForPublication1;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span:nth-of-type(1) span span")
	private WebElement textCheck1ForPublication2;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span:nth-of-type(2) span span")
	private WebElement textCheck2ForPublication2;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span:nth-of-type(3) span span")
	private WebElement textCheck3ForPublication2;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span")
	private WebElement textCheckForPublication3;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item--1'] span:nth-of-type(2)")
	private WebElement textCheckForPublication4;
	
	@FindBy(how = How.CSS, using = "li[id= 'article-list-item--3'] span:nth-of-type(2)")
	private WebElement textCheckForName1;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-1'] li[id='article-list-item--5'] span:nth-of-type(3)")
	private WebElement textCheckForName2;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item--7'] span:nth-of-type(2)")
	private WebElement textCheckForName3;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item--6'] span:nth-of-type(3) span span span")
	private WebElement textCheckForName4;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-parties-0'] span:nth-of-type(1) span")
	private WebElement textCheckForName5;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item-0'] span:nth-of-type(2)")
	private WebElement textCheckForName6;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-paginator-content yui3-paginator'] span[class='custom-page-report']")
	private WebElement resultCount;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-article-body-0'] span:nth-of-type(1) span span")
	private WebElement textCheckForInDepth2;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item--1'] span:nth-of-type(2) span")
	private WebElement textCheckForInDepth3;
	
	@FindBy(how = How.CSS, using = "div[id='case-article-byLine-0'] span span")
	private WebElement textCheckForInDepth4;
	
	@FindBy(how = How.CSS, using = "li[id='article-list-item--1']")
	private WebElement textCheckForInDepth5;
	
	@FindBy(how = How.CSS, using = "div[id='stream-container'] div[data-key='/research#/verdicts-settlements/select-results/&personId=1004059/display/Philip Corboy'] a")
	private WebElement moreButtonForJVRForPhilipCorboy;
	
	
	
	
	
	
	
	
	public WebElement getMoreButtonForJVRForPhilipCorboy(){
		
		return moreButtonForJVRForPhilipCorboy;
	}
	
	
	public WebElement getTextCheckForInDepth5(){
		
		return textCheckForInDepth5;
	}
	
	public WebElement getTextCheckForInDepth4(){
		
		return textCheckForInDepth4;
	}
	
	public WebElement getTextCheckForInDepth3(){
		
		return textCheckForInDepth3;
	}
	
	public WebElement getTextCheckForInDepth2(){
		
		return textCheckForInDepth2;
	}
	
	public WebElement getResultCount(){
		
		return resultCount;
	}
	
	public WebElement getTextCheckForName6(){
		
		return textCheckForName6;
	}
	
	public WebElement getTextCheckForName5(){
		
		return textCheckForName5;
	}
	
	public WebElement getTextCheckForName4(){
		
		return textCheckForName4;
	}
	
	public WebElement getTextCheckForName3(){
		
		return textCheckForName3;
	}
	
	public WebElement getTextCheckForName2(){
		
		return textCheckForName2;
	}
	
	public WebElement getTextCheckForName1(){
		
		return textCheckForName1;
	}
	
	public WebElement getTextCheckForPublication4(){
		
		return textCheckForPublication4;
	}
	
	public WebElement getTextCheckForPublication3(){
		
		return textCheckForPublication3;
	}
	
	public WebElement gettextCheck3ForPublication2(){
		
		return textCheck3ForPublication2;
	}
	
	public WebElement getTextCheck2ForPublication2(){
		
		return textCheck2ForPublication2;
	}
	
	public WebElement getTextCheck1ForPublication2(){
		
		return textCheck1ForPublication2;
	}
	
	public WebElement getTextCheckForPublication1(){
		
		return textCheckForPublication1;
	}
	
	public WebElement getTextCheckForDateRange(){
		
		return textCheckForDateRange;
	}
	
	public WebElement getTextSelectedCases(){
		
		return textSelectedCases;
	}
	
	public WebElement getTextCheckSelectAllForKeyword9(){
		
		return textCheckSelectALLForKeyword9;
	}
	
	public WebElement getSelectAllResults(){
		
		return selectAll;
	}
	
	public WebElement getTextCheck2ForFirstResultKeyword7(){
		
		return textCheck2ForFirstResultKeyword7;
	}
	
	public WebElement getTextCheck1ForFirstResultKeyword7(){
		
		return textCheck1ForFirstResultKeyword7;
	}
	
	public WebElement getTextCheck2ForFirstResultKeyword6(){
		
		return textCheck2ForFirstResultKeyword6;
	}
	
	public WebElement getTextCheck1ForFirstResultKeyword6(){
		
		return textCheck1ForFirstResultKeyword6;
	}
	
	public WebElement getTextCheckForFirstResultKeyword5(){
		
		return textCheckForFirstResultKeyword5;
	}
	
    public WebElement getTextCheckForFirstResultKeyword4(){
		
		return textCheckForFirstResultKeyword4;
	}
	
	public WebElement getContinueButton(){
		
		return continueButton;
	}
	
	public WebElement getTextCheckForFirstResultKeyword3(){
		
		return textCheckForFirstResultKeyword3;
	}
	
	public WebElement getTextCheckForFirstResultKeyword2(){
		
		return textCheckForFirstResultKeyword2;
	}
	
	public WebElement getTextCheckForFirstResultKeyword8(){
		
		return textCheckForFirstResultKeyword8;
	}
	
	public WebElement getFirstResultForKeyword(){
		
		return firstResultForKeyword;
	}
	
	public WebElement getFirstResultCheckBox(){
		
		return firstResultCheckBox;
		
	}
	
	public WebElement getSearchResults(){
		
		return searchResultsButton;
	}
	
	public WebElement getSelected(){
		
		return viewSelectedButton;
	}
	
	public WebElement getCaseCheckText(){
		
		return caseNumberCheckText;
	}

}
