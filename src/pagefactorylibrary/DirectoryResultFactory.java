package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class DirectoryResultFactory {
	@FindBy(how = How.XPATH, using = "//a[contains(@class, 'search-click-through')]")
	private WebElement firstResult;
	
	@FindBy(how = How.XPATH, using = "(//p [contains(@id, 'search-header-sub-title')])[1]")
	private WebElement resultCount;
	
	@FindBy(how = How.XPATH, using = "(//a)[18]")
	private WebElement firstResultOrganization;
	
	@FindBy( id="header-logo")
	private WebElement homeButton;
	
	@FindBy(id = "filter-0-0")
	private WebElement lawFirmFilter;
	
	@FindBy(id="filter-0-1")
	private WebElement govFilter;
	
	@FindBy(id ="filter-1-0")
	private WebElement cityFilter;
	
	
	public WebElement getHome(){
		return homeButton;
	}
	
	public WebElement getResultCount(){
		return resultCount;
	}
	
	
//	private int resultCount(){
//		DirectoryResultFactory dResults =  PageFactory.initElements(getDriver(), DirectoryResultFactory.class);
//		
//		WebElement resultCount = dResults.getResultCount();
//		String count = resultCount.getText().toString();
//		String count2 = count.substring(12);
//		count = count2.trim();
//		int results = Integer.parseInt(count);
//		return results;
//	}
	
//	public int getResultNumber(){
//		return resultTotal();
//	}
//	
	public WebElement getFirstResult(){
		return firstResult;
	}
	
	public WebElement getLawFirmFilter(){
		
		return lawFirmFilter;
	}
}
