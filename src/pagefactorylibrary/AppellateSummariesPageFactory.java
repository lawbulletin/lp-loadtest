package pagefactorylibrary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AppellateSummariesPageFactory{
	
	@FindBy(how = How.XPATH, using ="//li[@id='navRes']")
	private WebElement legalResearch;
	
	@FindBy(how = How.XPATH, using ="//a[contains(@id, 'subtab-app-summaries')]")
	private WebElement appellateSummaries;
	
	@FindBy (how = How.XPATH, using ="//input[contains(@class,'yui3-tokeninput-input')]")
	private WebElement judge; 
	
	@FindBy (how = How.ID, using ="app-summaries-run-advanced-search")
	private WebElement searchButton;
	
	@FindBy (how = How.ID, using ="app-summaries-reset-advanced-search-bottom")
	private WebElement resetButton;
	
	@FindBy (how = How.ID, using ="form-field-4")
	private WebElement keyword;
	
	
//	public WebElement navAppellateSummaries() {
//		legalResearch.click();
//		return appellateSummaries;
//	}
	
	public WebElement getJudge() {
		return judge;
	}
	
	public WebElement getSearchButton() {
		return searchButton;
	}
	
	public WebElement getResetButton() {
		return resetButton;
	}
	
	public WebElement getKeyWord() {
		return keyword;
	}

}
