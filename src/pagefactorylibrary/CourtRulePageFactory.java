package pagefactorylibrary;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CourtRulePageFactory {
	
	@FindBy (how = How.XPATH, using = "//li[@id='navRes']")
	private WebElement legalResearch; 

	@FindBy (how = How.XPATH, using = "//a[contains(@id, 'subtab-court-rules')]")
	private WebElement courtRule; 
		

	@FindBy(how = How.ID, using="form-field-0")
	private WebElement keywords; 
	
	@FindBy(how = How.ID, using="form-field-1")
	private List<WebElement> jursidiction; 
	
	@FindBy(how = How.ID, using="form-field-2")
	private WebElement ruleNumber; 
	
	@FindBy(how = How.ID, using="form-field-3")
	private WebElement ruleTitle; 
	
	@FindBy(how = How.ID, using="court-rules-run-advanced-search")
	private WebElement search; 
	
	@FindBy(how = How.ID, using="court-rules-reset-advanced-search-bottom")
	private WebElement reset;
	
	@FindBy(how = How.ID, using = "research-sub-tab")
	private WebElement legalReasearchNavigationBar;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-advanced-search-form-container']")
	private WebElement searchColumn;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-centerContainer']")
	private WebElement centerColumn;
	
	@FindBy(how = How.CSS, using = "div[class='cms-content'] p span")
	private WebElement headingTextAboveCenterColumn;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-subheading']")
	private WebElement subHeading;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules']")
	private WebElement federalCourtRulesList;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-0'] a")
	private WebElement rulesOfUnitedStatesSupremeCourt;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-1'] a")
	private WebElement federalRulesOfEvidence;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-2'] a")
	private WebElement federalRulesOfCivilProcedure;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-3'] a")
	private WebElement federalRulesOfCriminalProcedure;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-4'] a")
	private WebElement federalRulesOfAppellateProcedure;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-5'] a")
	private WebElement rulesOfUSCourOfAppealsFor7thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-6'] a")
	private WebElement federalBankruptcyRules;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-7'] a")
	private WebElement rulesOfUSDistrictCourtNorthern;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-8'] a")
	private WebElement rulesOfUSBankruptcyCourtNorthern;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-9'] a")
	private WebElement rulesOfUSDistrictCourtSouthern;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-10'] a")
	private WebElement rulesOfUSBankruptcyCourtSouthern;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-11'] a")
	private WebElement rulesOfUSDistrictCourtCentral;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-federal-court-rules'] ul li[id='{prefix}-list-item-12'] a")
	private WebElement rulesOfUSBankruptcyCourtCentral;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-0'] a")
	private WebElement rulesOfIllinoisSupremecourt;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-1'] a")
	private WebElement rulesOfAttorneyRegistryAndDisciplinaryCommission;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-2'] a")
	private WebElement illinoisRulesOfEvidence;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-3'] a")
	private WebElement illinoisCodeOfCivilProcedure;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-4'] a")
	private WebElement illinoisCodeOfCriminalProcedure;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-5'] a")
	private WebElement rulesOfIllinoisCourtOfClaims;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-6'] a")
	private WebElement appellateCourtFirstDistrict;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-7'] a")
	private WebElement appellateCourtSecondThroughFifth;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-8'] a")
	private WebElement appellateCourtSecondDistrict;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-9'] a")
	private WebElement appellateCourtFourthDistrict;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-10'] a")
	private WebElement circuitCourtOfCookCounty;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-11'] a")
	private WebElement ordersCircuitCourtOfCookCounty;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-12'] a")
	private WebElement circuitCourt1stCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-13'] a")
	private WebElement circuitCourt2ndCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-14'] a")
	private WebElement circuitCourt3rdCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-15'] a")
	private WebElement circuitCourt4thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-16'] a")
	private WebElement circuitCourt5thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-17'] a")
	private WebElement circuitCourt6thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-18'] a")
	private  WebElement circuitCourt7thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-19'] a")
	private WebElement circuitCourt8thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-20'] a")
	private WebElement circuitCourt9thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-21'] a")
	private WebElement circuitCourt10thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-22'] a")
	private WebElement circuitCourt11thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-23'] a")
	private WebElement circuitCourt12thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-24'] a")
	private WebElement circuitCourt13thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-25'] a")
	private WebElement circuitCourt14thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-26'] a")
	private WebElement circuitCourt15thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-27'] a")
	private WebElement circuitCourt16thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-28'] a")
	private WebElement circuitCourt17thCircuit;
	
	@FindBy(how = How.CSS, using = "div[id='court-rules-landing-state-court-rules'] ul li[id='{prefix}-list-item-29'] a")
	private WebElement circuitCourt18thCircuit;
	
	
	
	

	
	
	
	
	public WebElement getCircuitCourt16thCircuit(){
		
		return circuitCourt16thCircuit;
	}
	
	public WebElement getCircuitCourt15thCircuit(){
		
		return circuitCourt15thCircuit;
	}
	
	public WebElement getCircuitCourt14thCircuit(){
		
		return circuitCourt14thCircuit;
	}
	
	public WebElement getCircuitCourt13thCircuit(){
		
		return circuitCourt13thCircuit;
	}
	
	public WebElement getCircuitCourt12thCircuit(){
		
		return circuitCourt12thCircuit;
	}
	
	public WebElement getCircuitCourt11thCircuit(){
		
		return circuitCourt11thCircuit;
	}
	
	public WebElement getCircuitCourt10thCircuit(){
		
		return circuitCourt10thCircuit;
	}
	
	public WebElement getCircuitCourt9thCircuit(){
		
		return circuitCourt9thCircuit;
	}
	
	public WebElement getCircuitCourt8thCircuit(){
		
		return circuitCourt8thCircuit;
	}
	
	public WebElement getCircuitCourt7thCircuit(){
		
		return circuitCourt7thCircuit;
	}
	
	public WebElement getCircuitCourt6thCircuit(){
		
		return circuitCourt6thCircuit;
	}
	
	public WebElement getCircuitCourt5thCircuit(){
		
		return circuitCourt5thCircuit;
	}
	
	public WebElement getCircuitCourt4thCircuit(){
		
		return circuitCourt4thCircuit;
	}
	
	public WebElement getCircuitCourt3rdCircuit(){
		
		return circuitCourt3rdCircuit;
	}
	
	public WebElement getCircuitCourt2ndCircuit(){
		
		return circuitCourt2ndCircuit;
	}
	
	public WebElement getCircuitCourt1stCircuit(){
		
		return circuitCourt1stCircuit;
	}
	public WebElement getOrdersCircuitCourtOfCookCounty(){
		
		return ordersCircuitCourtOfCookCounty;
	}
	
	public WebElement getCircuitCourtOfCookCounty(){
		
		return circuitCourtOfCookCounty;
	}
	
	public WebElement getAppellateCourtFourthDistrict(){
		
		return appellateCourtFourthDistrict;
	}
	
	public WebElement getAppellateCourtSecondDistrict(){
		
		return appellateCourtSecondDistrict;
	}
	public WebElement getAppellateCourtSecondThroughFifth(){
		
		return appellateCourtSecondThroughFifth;
	}
	
	public WebElement getAppellateCourtFirstDistrict(){
		
		return appellateCourtFirstDistrict;
	}
	
	public WebElement getRulesOfIllinoisCourtOfClaims(){
		
		return rulesOfIllinoisCourtOfClaims;
	}
	
	public WebElement getIllinoisCodeOfCriminalProcedure(){
		
		return illinoisCodeOfCriminalProcedure;
	}
	
	public WebElement getIllinoisCodeOfCivilProcedure(){
		
		return illinoisCodeOfCivilProcedure;
	}
	
	public WebElement getIllinoisRulesOfEvidence(){
		
		return illinoisRulesOfEvidence;
	}
	
	public WebElement getRulesOFAttorneyRegistry(){
		
		return rulesOfAttorneyRegistryAndDisciplinaryCommission;
	}
	
	
	public WebElement getIllinoisSupremeCourt(){
		
		return rulesOfIllinoisSupremecourt;
	}
	
	public WebElement getRulesOFUSBankruptcyCourtCentral(){
		
		return rulesOfUSBankruptcyCourtCentral;
	}
	
	public WebElement getRulesOfUSDistrictCourtCentral(){
		
		return rulesOfUSDistrictCourtCentral;
	}
	
	public WebElement getRulesOfUSBankruptcyCourtSouthern(){
		
		return rulesOfUSBankruptcyCourtSouthern;
	}
	
	public WebElement getRulesOfUSDistrictCourtSouthern(){
		
		return rulesOfUSDistrictCourtSouthern;
		
	}
	
	public WebElement getRulesOfBankruptcyCourtNorthern(){
		
		return rulesOfUSBankruptcyCourtNorthern;
	}
	public WebElement getRulesOfUSDistrictCourtNorthern(){
		
		return rulesOfUSDistrictCourtNorthern;
	}
	
	public WebElement getFederalBankruptcyRules(){
		
		return federalBankruptcyRules;
	}
	public WebElement getRulesOf7thCircuit(){
		
		return rulesOfUSCourOfAppealsFor7thCircuit;
	}
	
	public WebElement getFederalRulesOfAppellateProcedure(){
		
		return federalRulesOfAppellateProcedure;
	}
	
	
	public WebElement getFederalRulesOfCriminalProcedure(){
		
		return federalRulesOfCriminalProcedure;
	}
	
	public WebElement getFederalRulesOfCivilProcedure(){
		
		return federalRulesOfCivilProcedure;
	}
	
	
	public WebElement getFederalRulesOFEvidence(){
		
		return federalRulesOfEvidence;
	}
	
	public WebElement getFederalCourtRulesList(){
		
		return federalCourtRulesList;
	}
	
	public WebElement getRulesOfUnitedStatesSupremeCourt(){
		
		return rulesOfUnitedStatesSupremeCourt;
	}
	
	public WebElement getSubHeading(){
		
		return subHeading;
	}
	
	public WebElement getHeadingText(){
		
		return headingTextAboveCenterColumn;
	}
	
	public WebElement getCenterColumn(){
		
		return centerColumn;
	}
	
	public WebElement getSearchColumn(){
		
		return searchColumn;
	}
	
	public WebElement getLegalReasearchNavigationBar(){
		
		return legalReasearchNavigationBar;
	}
	
	public WebElement courtRule() {
		legalResearch.click();
		return courtRule;
	}
	
	public WebElement getKeywords() {
		return keywords;
	}
	
	public List<WebElement> selectJurisdiction() {
		return jursidiction;
	}
	
	public WebElement getRuleNumber() {
		return ruleNumber;
	}
	
	public WebElement getRuleTitle() {
		return ruleTitle;
	}
	
	public WebElement getSearchButton() {
		return search;
	}
	
	public WebElement getRestButton() {
		return reset;
	}
}
