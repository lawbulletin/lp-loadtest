package pagefactorylibrary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;


//https://weblogs.java.net/blog/johnsmart/archive/2010/08/09/selenium-2web-driver-land-where-page-objects-are-king
//http://mestachs.wordpress.com/2012/08/13/selenium-best-practices/
public class OrgDirectoryPageFactory {

	@FindBy (how = How.XPATH, using = "//li[@id='navDir']")
	private WebElement getDirctory; 
	
	@FindBy (how = How.XPATH, using = "/li[contains(@id, 'org')]/a")
	private WebElement getOrg; 
	
	@FindBy (id = "form-field-1")
	private WebElement orgType;
	
	@FindBy(how = How.ID, using = "form-field-0")
	private WebElement orgName;
	
	@FindBy(how = How.ID, using = "form-field-3")
	private WebElement orgCity;
	
	@FindBy(id = "dir-org-run-advanced-search")
	private WebElement clickSearch;
	
	@FindBy(id = "dir-org-reset-advanced-search-bottom")
	private WebElement clickReset;
	
	@FindBy( id="dir-reset-advanced-search-bottom")
	private WebElement resetButton;
	
	@FindBy( id="practiceAreaDiv")
	private WebElement practiceArea;
	
	public WebElement getPracticcArea(){
		return practiceArea;
	}
	
	public WebElement getReset(){
		return resetButton;
	}
	
//	
//	public void navORG() {
//		getDirctory.click();
//		getOrg.click();		
//	}
	
	public Select getTypeSelect(){
		return new Select(orgType);
		//.selectByVisibleText("Germany");
	}
	
	public WebElement getOrgName( ) {
		return orgName;
	}
	public WebElement getOrgCity( ) {
		return orgCity;		
	}
		 
	public WebElement getSearch() {
		return clickSearch;
	}
	public WebElement getORGResetButton() {
		return clickReset;
	}

}




