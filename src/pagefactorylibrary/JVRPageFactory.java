package pagefactorylibrary;


import java.io.IOException;

import java.sql.SQLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


/**
 * 
 * @author hipatel & pthakkar
 *
 */
public class JVRPageFactory {
	
	WebElement publicationType;
	@FindBy(how = How.XPATH, using ="//li[@id='navRes']")
	private WebElement legalResearch;
	
	//@FindBy(how = How.XPATH, using ="//div[contains(@class, 'form-field-wrapper jv-keyword-search-div')]/input")
	@FindBy(how = How.XPATH, using ="//input[contains(@id, 'form-field-0')]")
	private WebElement keyword;
	
	@FindBy(how = How.ID, using ="form-field-1")
	private WebElement nameSearch;

	@FindBy(how = How.CSS, using ="input[id='form-field-2']")
	private WebElement caseNumber;	
	
	@FindBy(how = How.ID, using ="form-field-4")
	private WebElement startDate;
	
	@FindBy(how = How.ID, using ="form-field-5")
	private WebElement endDate;
	
	@FindBy(id = "jv-run-advanced-search")
	private WebElement search;

	@FindBy(how = How.ID, using ="jv-reset-advanced-search-bottom")
	private WebElement resetSearch;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-6'] option:nth-of-type(1)")
	private WebElement arbitration;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-6'] option:nth-of-type(2)")
	private WebElement newSuit;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-6'] option:nth-of-type(3)")
	private WebElement settlement;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-6'] option:nth-of-type(4)")
	private WebElement verdicts;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3']")
	private WebElement nameOptions;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(1)")
	private WebElement allCategories;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(2)")
	private WebElement defendant;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(3)")
	private WebElement defendantAttorney;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(4)")
	 private WebElement defendantExpert;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(5)")
	private WebElement defendantPhysician;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(6)")
	private WebElement judge;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(7)")
	private WebElement plaintiff;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(8)")
	private WebElement plaintiffAttorney;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(9)")
	private WebElement plaintiffExpert;
	
	@FindBy(how = How.CSS, using = "select[id='form-field-3'] option:nth-of-type(10)")
	private WebElement plaintiffPhysician;
	
	@FindBy(how = How.CSS, using = "a[id='add-jv-terms']")
	private WebElement advancedSearch;
	
	@FindBy(how = How.CSS, using = "div[id='jv-term-select-edit-left'] ul li")
	private List<WebElement> advancedCategoryList;
	
	@FindBy(how = How.CSS, using = "div[id='jv-term-select-edit-mid'] ul li")
	private List<WebElement> refineCategoryList;
	
	@FindBy(how = How.CSS, using = "div[id='jv-term-select-edit-right'] ul li")
	private List<WebElement> specificTermList;
	
	@FindBy(how = How.CSS, using = "a[id='jv-term-select-select-all']")
	private WebElement selectAll;
	
	@FindBy(how = How.CSS, using = "a[id='form-button-Save']")
	private WebElement saveCategories;

	
	
	
	
	
	
	
	
	public WebElement getSaveCategoriesButton(){
		
		return saveCategories;
	}
	
	public WebElement getSelectAllLink(){
		
		return selectAll;
	}
	
	public List<WebElement> getSpecificTermList(){
		
		return specificTermList;
	}
	
	public List<WebElement> getRefineCategoryList(){
		
		return refineCategoryList;
	}
	
	public List<WebElement> getAdvancedCategoryList(){
		
		return advancedCategoryList;
	}
	
	public WebElement getAdvancedSearch(){
		
		return advancedSearch;
	}
	
	public WebElement getPlaintiffPhysician(){
		
		return plaintiffPhysician;
	}
	
	public WebElement getPlaintiffExpert(){
		
		return plaintiffExpert;
	}
	
	public WebElement getPlaintiffAttorney(){
		
		return plaintiffAttorney;
	}
	
	public WebElement getPlaintiff(){
		
		return plaintiff;
	}
	
	public WebElement getJudge(){
		
		return judge;
	}
	
	public WebElement getdefendantPhysician(){
		
		return defendantPhysician;
	}
	
	
	public WebElement getDefendant(){
		
		
		return defendant;
	}
	
	public WebElement getAllCategories(){
		
		return allCategories;
	}
	
	
	public WebElement getDefendantExpert(){
		
		return defendantExpert;
	}
	
	
	public WebElement getDefendantAttorney(){
		
		return defendantAttorney;
	}
	
	public WebElement getNameOptions(){
		
		return nameOptions;
	}
	
	
	
	public WebElement getPublicationType(int selection){
		switch(selection)
		{
		
		case 1 :
			publicationType = arbitration;
			break;
		case 2 :
			publicationType = newSuit;
			break;
		case 3 :
			publicationType = settlement;
			break;
		case 4 :
			publicationType = verdicts;
			break;
		}
		
		return this.publicationType;
		
	}
	
	
	
	public WebElement getKeyword() {
		return keyword;
	}
	
	public WebElement getJVRSearchButton() {
		return search;
	}
	public WebElement getNameSearch() {
		return nameSearch;
	}
	public WebElement getCaseNumberSearch() {
		 return caseNumber;
	}
	public WebElement getStartDate() {
		return startDate;
	}
	
	public WebElement getEndDate() {
		return endDate; 
	}
	
	public WebElement getReset(){
		return resetSearch;
	}
//	public void clickJVRSearchButton() throws IOException, SQLException, Exception {		
//		try {
//			String uRL = "isSiteAdmin=true/";
//			String jVRURL = SeleniumDriver.getDriver().getCurrentUrl();
//			search.click();
//			//a pop up modal appears if not logged in as admin user, default search results display is 200.  
//			if (isElementPresent(By.className("modal-content-confirmation"))){
//				waitForElement(By.id("form-button-Submit")).click();
//				System.out.println("this account does not has admin acess");
//			} else if (jVRURL.contentEquals(uRL)) {
//				System.out.println("this account has admin acess");
//			}
//		} catch (Exception e) {
//			if(isElementPresent(By.id("//a[contains(.,'Return to the search form')]"))){
//				System.err.println("no results found ");
//			}
//			else if (isElementPresent(By.id("formError"))){
//				String errorText = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id,'formError')]/ul/ul/li")).getText();
//				System.err.println("an error has occur while searching: " + errorText);
//			} 
//		}	
//	}
	
	/**
	 * 
	 * @param category 	 JVR Search Category i.e Injury/Disease </p>
	 * @param subCategory   JVR Sub Category appears after category is selected i.e Injury/Disease > J-L - 32 </p>
	 * @param selectALL  	Boolean, True means it will select all of the Terms after Category adn SubCateogry are selected. 
	 * 						False means do not select all of the terms. NOTE, if False is selected you have to enter index number </p>
	 * @param index 		uses integer defined by user to randomly selects terms 
	 * @throws Exception
	 */
//	public void jVRCategories(String category, String subCategory, boolean selectALL,  String oneTerm) throws Exception {
//		waitForElement(By.xpath("//a[contains(@id,'add-jv-terms')]")).click(); // open 
////		ArrayList<String> saveSubCategoryList = new ArrayList<String>();  
////		ArrayList<String> saveTermsList = new ArrayList<String>();  
//		if (isElementPresent(By.className("modal-content"))){
//			 WebElement selectCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-left')]/ul"));
//			 List<WebElement> categoryOptions = selectCategory.findElements(By.tagName("li"));
//			    for(WebElement option : categoryOptions){
//			        if(option.getText().equals(category)) {
//			            option.click();
//			            break;
//			        }
//			    }
//		    WebElement selectSubCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-mid')]/ul"));
//			List<WebElement> categorySuboptions = selectSubCategory.findElements(By.tagName("li"));
////				trying to save the subcategory into list so i can print them of 
////				for (int i = 1; i == categorySuboptions.size();  i++ ){
////					String test = categorySuboptions.get(i).getText();
////					saveSubCategoryList.add(test);
////				}
////				System.out.println("Sub Cateogry List: " + saveSubCategoryList);
//		    	for(WebElement option : categorySuboptions){
//		    		if(option.getText().equals(subCategory)) {
//		    			option.click();
//		    			break;
//		    		}
//		    	}
//		    	
//		    	if (selectALL) {
//		    		//click select all 
//		    		waitForElement(By.id("jv-term-select-select-all")).click();
//		    	} else {
//		    			WebElement selectTerms = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-right')]/ul"));
//						List<WebElement> terms = selectTerms.findElements(By.tagName("li"));
//						for(WebElement option : terms){
//					        if(option.getText().equals(oneTerm)) {
//					            option.click();
//					            break;
//					        }
//						}
//		    		
//		    	}
//		}
//		waitForElement(By.id("form-button-Save")).click();
//	}
//	
	public void resetSearch(String searchTerm) {
		resetSearch.click();
	}
	
	
	
}

