package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewsResultFactory {
	
	@FindBy(how = How.CSS, using = "div[class='search-row row-detail first-result'] div p a")
	private WebElement firstResult;
	
	@FindBy(how = How.CSS, using = "div[id='leftContent2'] a[id='filter-0-1'] span[class='filter-check-box']")
	private WebElement pastWeekFilter;
	
	
	public WebElement getFirstResult(){
		
		return firstResult;
	}
	
	public WebElement getPastWeekFilter(){
		
		return pastWeekFilter;
	}

}
