package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LegalResearchPageFactory {

	@FindBy(how = How.CLASS_NAME, using ="sub-tab-non-link-title")
	private WebElement currentPageButton; 
	
	public WebElement getCurrentPageButton() {
		return currentPageButton;
	}
	
	@FindBy(id="subtab-court-dockets")
	private WebElement courtDockets; 
	
	public WebElement getCourtDockets() {
		return courtDockets;
	}
	
	@FindBy(id="subtab-verdicts-settlements")
	private WebElement jvs; 
	
	public WebElement getJVS() {
		return jvs;
	}
	
	@FindBy(id="subtab-app-summaries")
	private WebElement appSummaries; 
	
	public WebElement getAppSummaries() {
		return appSummaries;
	}
	
	@FindBy(id="subtab-court-rules")
	private WebElement courtRules; 
	
	public WebElement getCourtRules() {
		return courtRules;
	}
	
	@FindBy(id="subtab-judges")
	private WebElement judges; 
	
	public WebElement getJudges() {
		return judges;
	}
	
	@FindBy(id="subtab-resources")
	private WebElement otherResources; 
	
	public WebElement getOtherResources() {
		return otherResources;
	}
	
	@FindBy(id="subtab-statutes")
	private WebElement statutes; 
	
	public WebElement getStatutes() {
		return statutes;
	}
	
	@FindBy(id ="header-logo")
	private WebElement headerLogo;
	
	public WebElement getHeaderLogo() {
		return headerLogo;
	}
}
