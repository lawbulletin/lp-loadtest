package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CourtDocketResultFactory {
	
//	@FindBy(how = How.XPATH, using = "(//a)[17]")
//	private WebElement firstResult;
	
	@FindBy(how = How.CSS, using = "div[id='result-node'] div:nth-of-type(1) table tbody tr[class='yui3-datatable-even'] td[class='yui3-column-caseNumber'] div a")
	private WebElement firstResult;
	
	@FindBy(id = "subTabContent2")
	private WebElement noResultsBar;
	
	@FindBy(how = How.ID, using = "cd-back-form")
	private WebElement searchFormButton;
	
	@FindBy(how = How.ID, using = "resultCount")
	private WebElement resultCount;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-paginator-content yui3-paginator']")
	private WebElement totalSearchRecords;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-datatable-liner'] a[id='case-details-271567508']")
	private WebElement firstRecordCaseNumberSearch;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-datatable-liner'] a[id='case-details-515558']")
	private WebElement firstRecordJudgeSearch;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-datatable-liner'] a[id='case-details-245515581']")
	private WebElement firstRecordLawyerSearch;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-datatable-liner'] a[id='case-details-276862053']")
	private WebElement firstRecordPlaintiffDefendantSearch;
	
	@FindBy(how = How.CSS, using = "div[class='modal-content'] div[class='modal-buttons'] a")
	private WebElement okButtonCaseDetails;
	
	@FindBy(how= How.CSS, using = "div[id='court-dockets-result-header'] a[id='next']")
	private WebElement nextButtonPlaintiffSearchHeader;
	
	@FindBy(how= How.CSS, using = "div[id='court-dockets-result-header'] a[id='last']")
	private WebElement lastButtonPlaintiffSearchHeader;
	
	@FindBy(how= How.CSS, using = "div[id='court-dockets-result-header'] a[id='prev']")
	private WebElement previousButtonPlaintiffSearchHeader;
	
	@FindBy(how = How.CSS, using = "div[id='court-dockets-result-header'] a[id='first']")
	private WebElement firstButtonPlaintiffSearchHeader;
	
	@FindBy(how = How.CSS, using = "div[id='subTabContent3'] a[id='next']")
	private WebElement nextButtonPlaintiffSearchFooter;
	
	@FindBy(how = How.CSS, using = "div[id='subTabContent3'] a[id='last']")
	private WebElement lastButtonPlaintiffSearchFooter;
	
	@FindBy(how = How.CSS, using = "div[id='subTabContent3'] a[id='prev']")
	private WebElement previousButtonPlaintiffSearchFooter;
	
	@FindBy(how = How.CSS, using = "div[id='subTabContent3'] a[id='first']")
	private WebElement firstButtonPlaintiffSearchFooter;
	
	@FindBy(how= How.CSS, using = "a[class='yui3-paginator-next']")
	private WebElement nextButtonCaseSearch;
	
	@FindBy(how = How.CSS, using = "a[class='yui3-paginator-last']")
	private WebElement lastButtonCaseSearch;
	
	@FindBy(how = How.CSS, using = "a[class='yui3-paginator-previous']")
	private WebElement previousButtonCaseSearch;
	
	@FindBy(how = How.CSS, using = "a[class='yui3-paginator-first']")
	private WebElement firstButtonCaseSearch;
	
	@FindBy(how = How.CSS, using = "span[class='yui3-paginator-pages'] a[page='1']")
	private WebElement resultPage1;
	
	@FindBy(how = How.CSS, using = "span[class='yui3-paginator-pages'] a[page='2']")
	private WebElement resultPage2;
	
	@FindBy(how = How.CSS, using = "span[class='yui3-paginator-pages'] a[page='3']")
	private WebElement resultPage3;
	
	@FindBy(how = How.CSS, using = "span[class='yui3-paginator-pages'] a[page='4']" )
	private WebElement resultPage4;
	
	@FindBy(how = How.CSS, using = "span[class='yui3-paginator-pages'] a[page='5']")
	private WebElement resultPage5;
	
	@FindBy(how = How.CSS, using = "span[class='custom-page-report']")
	private WebElement resultCountCasePage;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] a[class='yui3-paginator-next']")
	private WebElement nextButtonFooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] a[class='yui3-paginator-last']")
	private WebElement lastButtonFooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] a[class='yui3-paginator-first']")
	private WebElement firstButtonFooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] a[class='yui3-paginator-previous']")
	private WebElement previousButtonFooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] span[class='yui3-paginator-pages'] a[page='2']")
	private WebElement page2FooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] span[class='yui3-paginator-pages'] a[page='1']")
	private WebElement page1FooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] span[class='yui3-paginator-pages'] a[page='3']")
	private WebElement page3FooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] span[class='yui3-paginator-pages'] a[page='4']")
	private WebElement page4FooterCaseSearch;
	
	@FindBy(how = How.CSS, using = "div[id='paginator2-result-node'] span[class='yui3-paginator-pages'] a[page='5']")
	private WebElement page5FooterCaseSearch;
	
	
	
	
	
	
	
	public WebElement getPage5FooterCaseSearch(){
		
		return page5FooterCaseSearch;
	}
	
	public WebElement getPage4FooterCaseSearch(){
		
		return page4FooterCaseSearch;
	}
	
	public WebElement getPage3FooterCaseSearch(){
		
		return page3FooterCaseSearch;
	}
	
	public WebElement getPage1FooterCaseSearch(){
		
		return page1FooterCaseSearch;
	}
	
	public WebElement getPage2FooterCaseSearch(){
		
		return page2FooterCaseSearch;
	}
	
	public WebElement getCaseSearchPreviousButtonFooter(){
		
		return previousButtonFooterCaseSearch;
	}
	
	public WebElement getCaseSearchFirstButtonFooter(){
		
		return firstButtonFooterCaseSearch;
	}
	
	public WebElement getCaseSearchLastButtonFooter(){
		
		return lastButtonFooterCaseSearch;
	}
	
	public WebElement getCaseSearchNextButtonFooter(){
		
		return nextButtonFooterCaseSearch;
	}
	
	public WebElement getCaseSearchCount(){
		
		return resultCountCasePage;
	}
	
	public WebElement getResultPage5(){
		
		return resultPage5;
	}
	
	public WebElement getResultPage4(){
		
		return resultPage4;
	}
	
	public WebElement getResultPage3(){
		
		return resultPage3;
	}
	
	public WebElement getResultPage2(){
		
		return resultPage2;
	}
	
	public WebElement getResultPage1(){
		
		return resultPage1;
	}
	
	
	public WebElement getFirstButtonCaseSearch(){
		
		return firstButtonCaseSearch;
	}
	
	public WebElement getPreviousButtonCaseSearch(){
		
		return previousButtonCaseSearch;
	}
	
	public WebElement getNextButtonCaseSearch(){
		
		return nextButtonCaseSearch;
	}
	
	public WebElement getLastButtonCaseSearch(){
		
		return lastButtonCaseSearch;
	}
	
	public WebElement getFirstButtonPlaintiffSearchFooter(){
		
		return firstButtonPlaintiffSearchFooter;
	}
	
	public WebElement getPreviousButtonPlaintiffSearchFooter(){
		
		return previousButtonPlaintiffSearchFooter;
	}
	
	public WebElement getLastButtonPlaintiffSearchFooter(){
		
		return lastButtonPlaintiffSearchFooter;
	}
	
	public WebElement getNextButtonPlaintiffSearchFooter(){
		
		return nextButtonPlaintiffSearchFooter;
	}
	
	public WebElement getFirstButtonPlaintiffSearchHeader(){
		
		return firstButtonPlaintiffSearchHeader;
	}
	
	public WebElement getPreviousButtonPlaintiffSearchHeader(){
		
		return previousButtonPlaintiffSearchHeader;
	}
	
	public WebElement getLastButtonPlaintiffSearchHeader(){
		
		return lastButtonPlaintiffSearchHeader;
	}
	
	public WebElement getNextButtonPlaintiffSearchHeader(){
		
		return nextButtonPlaintiffSearchHeader;
	}
	
	public WebElement getOkButton(){
		
		return okButtonCaseDetails;
	}
	
	
	public WebElement getCDFirstResult(){
		return firstResult;
	}
	
	public WebElement getNoResultsBar(){
		
		return noResultsBar;
	}
	
	public WebElement getSearchFormButton(){
		
		return searchFormButton;
	}
	
	public WebElement getResultCount(){
		
		return resultCount;
	}
	
	public WebElement getTotalRecords(){
		
		return totalSearchRecords;
	}
	
	public WebElement getFirstRecordCaseNumberSearch(){
		
		return firstRecordCaseNumberSearch;
	}
	
	public WebElement getFirstRecordJudgeSearch(){
		
		return firstRecordJudgeSearch;
	}
	
	public WebElement getFirstRecordLawyerSearch(){
		
		return firstRecordLawyerSearch;
	}
	
	public WebElement getFirstRecordPlaintiffDefendantSearch(){
		
		return firstRecordPlaintiffDefendantSearch;
	}
}
