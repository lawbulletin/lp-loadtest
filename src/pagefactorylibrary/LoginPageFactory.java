package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * 
 * @author hipatel
 *
 */
		

public class LoginPageFactory {

	@FindBy (id = "clientCode")
	private WebElement client;
	
	@FindBy(how = How.ID, using = "username")
	private WebElement username;

	@FindBy(how = How.ID, using = "password")
	private WebElement password;

	@FindBy(how = How.NAME, using = "submitButton")
	private WebElement submitButton;
	
	@FindBy(how = How.ID, using = "resetButton")
	private WebElement resetButton;
	
	@FindBy(how = How.CLASS_NAME, using = "login-forgot-password")
	private WebElement forgotPassword;
	
	@FindBy(how = How.ID, using = "my-profile-admin")
	private WebElement adminProfile;
	
	@FindBy(how = How.ID, using = "my-profile-logout")
	private WebElement logout;
	
	@FindBy(how = How.ID, using = "button-welcome-link")
	private WebElement welcomeLinkButton;
	
	@FindBy (how = How.ID, using ="my-profile-edit")
	private WebElement editProfile;
	
	@FindBy(how = How.XPATH, using = "(//p)[2]")
	private WebElement welcomeContent;
	
	@FindBy(id="login-create-act-heading")
	private WebElement troubleMessage;
	
//	@FindBy(how = How.XPATH, using = "//div[contains (@id, 'msg')]")
//	private WebElement nulMessages;
	
	public WebElement getTrouble(){
		return troubleMessage;
	}
	
	public WebElement getWelcomeContent() {
		return welcomeContent;
	}
	
	public WebElement getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public WebElement getPassword() {
		return password;
	}

	
	public WebElement getResetButton() {
		return resetButton;
	}

	
	public WebElement getSubmitButton() {
		return submitButton;
	}

	/**
	 * @return the forgotPassword
	 */
	public WebElement getForgotPassword() {
		return forgotPassword;
	}

	
	public WebElement getEditProfile(){
		return editProfile;
	}
	/**
	 * @return the adminProfile
	 */
	public WebElement getAdminProfile() {
		return adminProfile;
	}
	
	/**
	 * @return the welcomeLinkButton
	 */
	public WebElement getWelcomeLinkButton() {
		return welcomeLinkButton;
	}
	
	
	public WebElement getLogoutButton(){
		return logout;
	}
	
	public WebElement getClient(){
		return client;
	}
}

