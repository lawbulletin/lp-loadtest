package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ProfilePageFactory {
	
	@FindBy(id = "persons-bio-heading")
	private WebElement bioHeader;
	
	@FindBy (how = How.XPATH, using = "(//div[contains(@id, 'streaming-JV-list')]/div[contains(@class, 'streaming-content-item')]/div)[1]/a")
	private WebElement JVRTitle1;
	
	@FindBy (how = How.XPATH, using = "(//div[contains(@id, 'streaming-JV-list')]/div[contains(@class, 'streaming-content-item')]/div)[3]/a")
	private WebElement JVRTitle2;
	
	@FindBy(how = How.CLASS_NAME, using = "profile-title")
	private WebElement profileTitle;
	
	@FindBy(id = "pA-toc-contact-info")
	private WebElement contactInfo;
	
	@FindBy(id = "pA-toc-biography")
	private WebElement biography;
	
	@FindBy(id = "pA-toc-practice-areas")
	private WebElement practiceAreas;
	
	@FindBy(id ="pA-toc-industries" )
	private WebElement industries;
	
	@FindBy(id = "pA-toc-cases" )
	private WebElement casesAndMatters;
	
	@FindBy(id ="pA-toc-news" )
	private WebElement newsPublSpeaking;
	
	@FindBy(id ="pA-toc-affiliations" )
	private WebElement affiliationsAndHonors;
	
	@FindBy(id = "pA-toc-work-history")
	private WebElement workHistory;
	
	@FindBy(id = "pA-toc-admissions" )
	private WebElement admissionsAndEd;
	
	@FindBy(id = "button-title-edit")
	private WebElement editName;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@id, 'button-persons-contact-edit-')])[1]")
	private WebElement  editPrimaryContact;
	
	@FindBy(id = "button-persons-contact-add")
	private WebElement addContact;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@id, 'button-persons-contact-edit-')])[2]")
	private WebElement editOtherContact;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, 'button-persons-contact-del-')]")
	private WebElement  deleteContact ;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, 'button-persons-contact-primary')]")
	private WebElement makePrimary;
	
	@FindBy(id = "button-portrait-edit")
	private WebElement  editPicture;
	
	@FindBy(id = "button-persons-bio-dob-edit")
	private WebElement editDOB;
	
	@FindBy(id = "button-persons-bio-pob-edit")
	private WebElement editPOB;
	
	@FindBy(id = "button-persons-bio-lang-edit")
	private WebElement editLanguages;
	
	@FindBy(id = "button-persons-bio-bio-edit")
	private WebElement editBio;
	
	@FindBy(id="person-case-case-org-link-0")
	private WebElement caseOrgLink1;
	
	public WebElement getCaseOrgLink1(){
		return caseOrgLink1;
	}
	
	public WebElement getCasesAndMatters(){
		return casesAndMatters;
	}
	public WebElement getJVRTitle1(){
		return JVRTitle1;
	}
	
	public WebElement getJVRTitle2(){
		return JVRTitle2;
	}
	
	public WebElement getBioHeader() {
		return bioHeader;
	}

	public WebElement getProfileTitle() {
	return profileTitle;
	}

}
