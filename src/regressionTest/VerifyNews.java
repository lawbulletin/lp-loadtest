package regressionTest;

import org.junit.*;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;
import pagefactorylibrary.LPMainPageFactory;
import pagefactorylibrary.NewsPageFactory;
import pagefactorylibrary.NewsResultFactory;
import seleniumWebDriver.SeleniumDriver;

public class VerifyNews extends AbstractSprint {

	NewsPageFactory news = PFCaller.newsFactory();
	NewsResultFactory results = PFCaller.newsResultFactory();
	LPMainPageFactory main = PFCaller.lpMainFactory();

	
//	public void testSetup() {
//		
//		logIn();
//		main.getNewsLink().click();
//		forceWait(2);
//	}

//	@After
//	public void endTest() {
//		main.getHomeIcon().click();
//		forceWait(1);
//		System.out.println("Test is complete");
//	}

	@Test
	public void testForLogos() throws Exception {
		//logIn();
		forceWait(2);
		main.getNewsLink().click();
		System.out.println("Test for Logos");
		
		forceWait(2);

		if (news.getCldbLogo().isDisplayed()) {
			System.out.println("Chicago Daily Law Bulletin is visible");
			news.getCldbLogo().click();
			String cdlbURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
			if(cdlbURL.contains("publicationType=LB")){
			System.out.println("Link for CDLB works.");
			}
			main.getNewsLink().click();
		}
		
		forceWait(2);
		
		if(news.getChicagoLawyerLogo().isDisplayed()){
			System.out.println("Chicago Lawyer & Info Logo is visible");
			news.getChicagoLawyerLogo().click();
			forceWait(2);
			String clURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
			if(clURL.contains("publicationType=CL")){
				System.out.println("Link for CL works.");
			}
			main.getNewsLink().click();
			
		}
		
		forceWait(2);
		
		if(news.getJuryVerdictLogo().isDisplayed()){
			System.out.println("Jury verdict is visible");
			news.getJuryVerdictLogo().click();
			forceWait(2);
			String jvURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
			if(jvURL.contains("verdicts-settlements")){
				System.out.println("Link for JuryVerdict works.");
			}
			main.getNewsLink().click();
		}
		
		forceWait(2);
		
		if(news.getCourtNewsLogo().isDisplayed()){
			System.out.println("News From the Court is visible");
			news.getCourtNewsLogo().click();
			forceWait(2);
			String courtNewsURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
			if(courtNewsURL.contains("courtnews")){
				System.out.println("Link for CourtNews works.");
			}
			main.getNewsLink().click();
		}
		
		forceWait(2);
		
		if(news.getAppellateCS().isDisplayed()){
			System.out.println("Appellate Case Summaries is visible");
			news.getAppellateCS().click();
			forceWait(2);
			String appellateURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
			if(appellateURL.contains("app-summaries")){
				System.out.println("Link for Appellate Case Summaries works.");
			}
			main.getNewsLink().click();
		}
		
		forceWait(2);
		
		if(news.getItn().isDisplayed()){
			System.out.println("In the News is visible");
			news.getItn().click();
			forceWait(2);
			String itnURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
			if(itnURL.contains("subType=STYPIN")){
				System.out.println("Link for In the News works.");
			}
			main.getNewsLink().click();
		}
		
		forceWait(2);
		
		if(news.getJobLink().isDisplayed()){
			System.out.println("Jobs is visible");
//			news.getJobLink().click();
//			String jobURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
//			if(jobURL.contains("publicationType=CL")){
//				System.out.println("Link for CL works.");
//			}
		}
		
	}
	

	@Test
	public void testForSearch() {
		System.out.println("Test for searching news");
		main.getNewsLink().click();
		forceWait(2);
		news.getKeywords().click();
		news.getKeywords().clear();
		System.out.println("Searching with keyword LAW");
		news.getKeywords().sendKeys("Law");
		news.getSearch().click();
		forceWait(2);
		news.getContinueButton().click();
		forceWait(2);
		results.getPastWeekFilter().click();
		String currentURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
		if(currentURL.contains("dateRange=WEEK")){
		System.out.println("Past Week Filter selected");
		}
		results.getFirstResult().click();
		
		//tearDown();
		
	}

}
