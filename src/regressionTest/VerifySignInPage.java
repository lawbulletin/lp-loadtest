package regressionTest;

import java.util.ArrayList;
import java.util.List;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pagefactorylibrary.HomePageFactory;
import pagefactorylibrary.LoginPageFactory;
import seleniumWebDriver.SeleniumDriver;

public class VerifySignInPage extends AbstractSprint{
	private LoginPageFactory log = PFCaller.loginFactory();
	private HomePageFactory home = PFCaller.homeFactory();

	@Test
	public void signInPage(){
		System.out.println("Verify Sign-In Button links to Log-In Page");
		home.getsignInButton().click();
		forceWait(2);
		String current = SeleniumDriver.getDriver().getCurrentUrl().toString();
		if(current.contains("lb-cas/login")){
			System.out.println("Pass");
		}
		else{System.err.println("Fail");}
	}
	
	@Test
	public void verifyFieldsUserName() throws Exception{
		System.out.println("Verify Username field Enabled");
		condition1 = log.getUsername().isEnabled();
		tfTest();
	}
	
	@Test
	public void verifyFieldsPassword() throws Exception{
		System.out.println("Verify Password Field Enabled");
		condition1 = log.getPassword().isEnabled();
		tfTest();
	}
	
	@Test
	public void verifyFieldsClient() throws Exception{
		System.out.println("Verify Client Field Enabled");
		condition1 = log.getClient().isEnabled();
		tfTest();
	}
	
	@Test
	public void verifyButtonClear() throws Exception{
		System.out.println("Verify Clear Button is Enabled");
		condition1 = log.getSubmitButton().isEnabled();
		tfTest();
	}
	
	@Test
	public void verifyButtonLogin() throws Exception{
		System.out.println("Verify Login Button is Enabled");
		condition1 = log.getResetButton().isEnabled();
		tfTest();
	}
	
	@Test
	public void verifyForgotPassword() throws Exception{
		System.out.println("Verify Forgot Password Link Navigates to Correct Page");
		log.getForgotPassword().click();
		forceWait(2);
		String url = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition1 = url.contains("reset-request/");
		SeleniumDriver.getDriver().navigate().back();
		forceWait(2);
		tfTest();
	}
	
//	@Test
//	public void verifyTroubleContent() throws Exception{
//		System.out.println("Verify Trouble Logging In Web Content");
//		e =log.getTrouble();
//		String trouble = e.getText().toString();
//		System.out.println(trouble);
//		condition1 = trouble.contains("Trouble");
//		tfTest();
//	}
	
	@Test
	public void verifyWelcomContent() throws Exception{
		System.out.println("Verify Welcome Content");
		String welcome = log.getWelcomeContent().getText().toString();
		System.out.println(welcome);
		welcome = welcome.toLowerCase();
		
		condition1 = welcome.contains("welcome");
		tfTest();
	}
	
	@Test
	public void verifyNullMessage() throws Exception{
		System.out.println("Verify Null Fields Error Messages");
		log.getResetButton().click();
		log.getSubmitButton().click();
		forceWait(2);
		List<WebElement> nullMessages = SeleniumDriver.getDriver().findElements(By.xpath("//div[contains (@id, 'msg')]"));
		
		List<String> messages = new ArrayList<String>();
		
		for(WebElement m : nullMessages){
			String msg = m.getText().toString();
			messages.add(msg);
		}

		for(String s : messages)
			if(!s.contains("required field")){
				System.err.println("Fail");
			}
			else{System.out.println("Expected message: " + s + "   - Pass");}
		
		System.out.println("------------------");
		
//		SeleniumDriver.getDriver().close();
		//SeleniumDriver.getDriver().quit(); PP
	}
}
