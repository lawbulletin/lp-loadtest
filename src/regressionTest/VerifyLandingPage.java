package regressionTest;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;

import org.junit.Test;

import pagefactorylibrary.HomePageFactory;
import seleniumWebDriver.SeleniumDriver;

public class VerifyLandingPage extends AbstractSprint{
private HomePageFactory home = PFCaller.homeFactory();

	@Test
	public void lpLogo() throws Exception{
		System.out.println("Verifying Presenct of Lawyerport Logo");
		condition1 = home.getHeaderLogo().isDisplayed();
		tfTest();
	}
	
	@Test
	public void signInTest() throws Exception{
		System.out.println("Verifying Presence of Sign-In Button");
		condition1 = home.getsignInButton().isDisplayed();
		tfTest();
	}
	
	@Test
	public void primaryImage() throws Exception{
		System.out.println("Verifying Presence of Primary Center Image");
		condition1 = home.getImageContainer().isDisplayed();
		tfTest();
	}
	
	//@Test
	//public void directoryWebContet() throws Exception{
	//	System.out.println("Verifying Presence of Direcotry Web Content Container");
	//	condition1 = home.getDirectoryBox().isDisplayed();
	//	tfTest();
	//	tearDown();
	//}
	
	@Test
	public void newsWebContet() throws Exception{
		System.out.println("Verifying Presence of News Web Content Container");
		condition1 = home.getNewsBox().isDisplayed();
		tfTest();
	}
	
	@Test
	public void legalResearchWebContent() throws Exception{
		System.out.println("Verifying Presence of Legal Researcg Web Content Container");
		condition1 = home.getLegalResearchBox().isDisplayed();
		tfTest();
	}
	
	@Test
	public void clickDirectoryContent() throws Exception{
		System.out.println("Verify that the user is taken to a page with Directory content and can go back to the home page");
		
		String homepage = SeleniumDriver.getDriver().getCurrentUrl().toString();
		home.getDirectoryInfoButton().click();
		forceWait(2);
		String dirInfoPage = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition1 = dirInfoPage.contains("directory-learn-more");
		SeleniumDriver.getDriver().navigate().back();
		forceWait(2);
		String home = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition2 = home.contentEquals(homepage);
		
		if(condition1 && condition2){
			System.out.println("Pass");
		}
		else{
			System.err.println("Fail");
			System.out.println(dirInfoPage);
			System.out.println(home);
			System.out.println(homepage);
			
		}
		System.out.println("------------------");
	}
	
	@Test
	public void clickNewsContent() throws Exception{
		System.out.println("Verify that the user is taken to a page with News content and can go back to the home page");
		
		String homepage = SeleniumDriver.getDriver().getCurrentUrl().toString();
		home.getNewsInfoButton().click();
		forceWait(2);
		String newsInfoPage = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition1 = newsInfoPage.contains("news-learn-more");
		SeleniumDriver.getDriver().navigate().back();
		forceWait(2);
		String home = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition2 = home.contentEquals(homepage);
		
		if(condition1 && condition2){
			System.out.println("Pass");
		}
		else{
			System.err.println("Fail");
			System.out.println(newsInfoPage);
			System.out.println(home);
			System.out.println(homepage);
			
		}
		
		System.out.println("------------------");
	}
	
	@Test
	public void clickLRContent() throws Exception{
		System.out.println("Verify that the user is taken to a page with Legal Research content and can go back to the home page");
		
		String homepage = SeleniumDriver.getDriver().getCurrentUrl().toString();
		home.getLRInfoButton().click();
		forceWait(2);
		String lrInfoPage = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition1 = lrInfoPage.contains("research-learn-more");
		SeleniumDriver.getDriver().navigate().back();
		forceWait(2);
		String home = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition2 = home.contentEquals(homepage);
		
		if(condition1 && condition2){
			System.out.println("Pass");
		}
		else{
			System.err.println("Fail");
			System.out.println(lrInfoPage);
			System.out.println(home);
			System.out.println(homepage);
			
		}
		
		System.out.println("------------------");
	}
		
}
